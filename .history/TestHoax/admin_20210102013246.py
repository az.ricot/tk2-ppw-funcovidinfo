from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(JawabanUser)
admin.site.register(Kegiatan)
admin.site.register(UserKegiatan)
