from django import forms
from .models import JawabanUser, UserKegiatan, Kegiatan

class FormSoal(forms.ModelForm):

    class Meta:
        model = JawabanUser
        fields = ['Jawaban']
        
        widgets = {
               'Jawaban': forms.RadioSelect(attrs={'class': 'Test123'})
           }

class FormPeserta(forms.ModelForm):
    class Meta:
        model = UserKegiatan
        fields = ['nama', 'kegiatan']

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama']
    
    widgets = {
        'nama': forms.TextInput(attrs={'class':'Test123'})
    }