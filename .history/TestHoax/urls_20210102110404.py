from django.urls import path

from . import views

app_name = 'TestHoax'

urlpatterns = [
    path('', views.testhoax, name='testhoax'),
    path('soal/', views.soal, name='soal'),
    path('soal2/<int:nomor>/<int:skor>', views.soal, name='soal2'),
    path('soal3/<int:nomor>/<int:skor>', views.soal, name='soal3'),
    path('soal4/<int:nomor>/<int:skor>', views.soal, name='soal4'),
    path('soal5/<int:nomor>/<int:skor>', views.soal, name='soal5'),
    path('skorhasil/<int:nomor>/<int:skor>', views.skor, name='skor'),
    path('fiturbaru/', views.fiturbaru, name='fiturbaru'),
    path('fiturbaru/ikutkegiatan/', views.ikutkegiatan, name='ikutkegiatan'),
    path('fiturbaru/ikutkegiatan/data/', views.olahdata, name='data'),
    path('fiturbaru/buatkegiatan/', views.buatkegiatan, name='buatkegiatan'),
    path('deletekegiatan/<str:pk>', views.deletekegiatan, name='deletekegiatan'),
    path('fiturbaru/daftarkegiatan/', views.daftarkegiatan, name='daftarkegiatan'),
    path('fiturbaru/ikutkegiatan/cekdata/', views.dekdata, name='cekdata'),
    
]
