from django.test import TestCase, Client
from .models import JawabanUser
from django.urls import reverse
from django.contrib.auth.models import User

# Create your tests here.
class TestWeb(TestCase):

    

    def test_template_home(self):
        response = Client().get('/TestHoax/')
        self.assertTemplateUsed(response, "home2.html")

    def test_models_JawabanUser(self):
        var = JawabanUser.objects.create(Jawaban="select1", Jawaban_ID=1)
        self.assertEquals(var.Jawaban, "select1")
        self.assertEquals(var.Jawaban_ID, 1)

    def test_url_home_test_hoaks(self):
        response = Client().get('/TestHoax/')
        self.assertEquals(response.status_code, 200)

    def test_konten_home_test_hoaks(self):
        response = Client().get('/TestHoax/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Selamat Datang di Test Hoax !", html_kembalian)
        self.assertIn("Ikuti Test", html_kembalian)
        
    
    def test_url_soal1(self):
        response = Client().get('/TestHoax/soal/')
        self.assertEquals(response.status_code, 200)
    
    def test_template_soal1(self):
        response = Client().get('/TestHoax/soal/')
        self.assertTemplateUsed(response, "TestHoax/Soal.html")

    def test_konten_soal1(self):
        response = Client().get('/TestHoax/soal/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Perhatikan Gambar Berikut Ini !", html_kembalian)
        

    def test_url_soal2(self):
        response = Client().get(reverse('TestHoax:soal2', kwargs={'nomor':2, 'skor':20}))
        self.assertEquals(response.status_code, 200)

    def test_template_soal2(self):
        response = Client().get(reverse('TestHoax:soal2', kwargs={'nomor':2, 'skor':20}))
        self.assertTemplateUsed(response, "Soal2.html")
        

    def test_konten_soal2(self): 
        response = Client().get(reverse('TestHoax:soal2', kwargs={'nomor':2, 'skor':20}))
        html_kembalian=response.content.decode('utf8')
        self.assertIn("Apakah berita ini termasuk berita benar/hoaks ?", html_kembalian)
        
        

    def test_url_soal3(self):
        response = Client().get(reverse('TestHoax:soal3', kwargs={'nomor':3, 'skor':20}))
        self.assertEquals(response.status_code, 200)

    def test_template_soal3(self):
        response = Client().get(reverse('TestHoax:soal3', kwargs={'nomor':3, 'skor':20}))
        self.assertTemplateUsed(response, "Soal3.html")

    def test_konten_soal3(self): 
        response = Client().get(reverse('TestHoax:soal3', kwargs={'nomor':3, 'skor':20}))
        html_kembalian=response.content.decode('utf8')
        self.assertIn("Apakah Postingan tersebut termasuk berita benar/hoaks ?", html_kembalian)

    def test_url_soal4(self):
        response = Client().get(reverse('TestHoax:soal4', kwargs={'nomor':4, 'skor':20}))
        self.assertEquals(response.status_code, 200)

    def test_template_soal4(self):
        response = Client().get(reverse('TestHoax:soal4', kwargs={'nomor':4, 'skor':20}))
        self.assertTemplateUsed(response, "Soal4.html")

    def test_konten_soal4(self): 
        response = Client().get(reverse('TestHoax:soal4', kwargs={'nomor':4, 'skor':20}))
        html_kembalian=response.content.decode('utf8')
        self.assertIn("Apakah postingan tersebut termasuk berita benar/hoaks ?", html_kembalian)

    def test_url_soal5(self):
        response = Client().get(reverse('TestHoax:soal5', kwargs={'nomor':5, 'skor':20}))
        self.assertEquals(response.status_code, 200)

    def test_template_soal5(self):
        response = Client().get(reverse('TestHoax:soal5', kwargs={'nomor':5, 'skor':20}))
        self.assertTemplateUsed(response, "Soal5.html")

    def test_konten_soal5(self): 
        response = Client().get(reverse('TestHoax:soal5', kwargs={'nomor':5, 'skor':20}))
        html_kembalian=response.content.decode('utf8')
        self.assertIn("Apakah postingan ini termasuk berita benar/hoaks ?", html_kembalian)

    def test_url_skor(self):
        response = Client().get(reverse('TestHoax:skor', kwargs={'nomor':5, 'skor':100}))
        self.assertEquals(response.status_code, 200)

    def test_template_skor(self):
        response = Client().get(reverse('TestHoax:skor', kwargs={'nomor':5, 'skor':100}))
        self.assertTemplateUsed(response, "TestHoax/skor.html")

    def test_konten_benar_skor(self): 
        response = Client().get(reverse('TestHoax:skor', kwargs={'nomor':5, 'skor':100}))
        html_kembalian=response.content.decode('utf8')
        self.assertIn("Kamu sudah menjawab sebagian besar pertanyaan dengan benar", html_kembalian)

    def test_konten_salah_skor(self): 
        response = Client().get(reverse('TestHoax:skor', kwargs={'nomor':5, 'skor':20}))
        html_kembalian=response.content.decode('utf8')
        self.assertIn("Hmm... Sepertinya kamu masih belum bisa membedakan antara berita benar dan hoaks nihh", html_kembalian)