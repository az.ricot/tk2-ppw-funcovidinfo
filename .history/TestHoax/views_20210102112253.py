from django.shortcuts import render, redirect
from .forms import FormSoal, FormKegiatan, FormPeserta
from .models import JawabanUser, Kegiatan, UserKegiatan
from django.urls import reverse
from django.http import HttpResponse
from django.http import JsonResponse
import json
import requests

# Create your views here.


def testhoax(request):
    return render(request, 'home2.html')

def soal(request, nomor=1, skor=0):
    check2= True
    check =True
    if request.method == "POST":
        jawabanuser= JawabanUser(Jawaban_ID=nomor)
        form = FormSoal(request.POST, instance=jawabanuser)
        if form.is_valid() and request.POST:
            form.save()
            jawaban = JawabanUser.objects.last()
            check= Cekjawaban(jawaban.Jawaban_ID, jawaban.Jawaban)
            JawabanUser.objects.last().delete()
            check2 = False
            

            if check:
                skor+=20
                nomor+=1
                context = {'check':check, 'check2':check2, 'skor':skor, 'nomor':nomor}
                

            else:
                nomor+=1
                context = {'check':check, 'check2':check2, 'skor':skor, 'nomor':nomor}

            if nomor ==2: #mulai dari 2 karena defaultnya nomornya satu
                return render(request, 'TestHoax/Soal.html', context)

            elif nomor ==3:
                return render(request, 'Soal2.html', context)

            elif nomor ==4:
                return render(request, 'Soal3.html', context)
    
            elif nomor ==5:
                return render(request, 'Soal4.html', context)

            elif nomor ==6:
                return render(request, 'Soal5.html', context)

    else:
        form = FormSoal()

    context = {'form':form, 'check':check, 'check2':check2, 'nomor':nomor, 'skor':skor}

    # klo tidak terjadi post
    if nomor ==1: #mulai dari satu karena nomor tidak di increment
        return render(request, 'TestHoax/Soal.html', context)

    elif nomor ==2:
        return render(request, 'Soal2.html', context)

    elif nomor ==3:
        return render(request, 'Soal3.html', context)

    elif nomor ==4:
        return render(request, 'Soal4.html', context)
    
    elif nomor ==5:
        return render(request, 'Soal5.html', context)

    return render(request, 'TestHoax/Soal.html', context)

def Cekjawaban(jawabanid, jwb):
    if jawabanid == 1:
        if jwb == "select1":
            return True

        else:
            return False

    elif jawabanid == 2:
        if jwb == "select2":
            return True

        else:
            return False

    elif jawabanid == 3:
        if jwb == "select2":
            return True

        else:
            return False
    
    elif jawabanid == 4:
        if jwb == "select2":
            return True

        else:
            return False
    
    elif jawabanid == 5:
        if jwb == "select2":
            return True

        else:
            return False

def skor (request, nomor, skor):
    if skor <60:
        check = False
    else:
        check = True
    context = {'nomor':nomor, 'skor':skor, 'check': check}
    return render(request, "TestHoax/skor.html", context)

def fiturbaru(request):
    return render(request, "TestHoax/fiturbaru.html")

def ikutkegiatan(request):
    
    if request.method=="POST":
        form = FormPeserta(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            form.save()
            form = FormPeserta()

    else:
        form = FormPeserta()

    context = {'form':form}
    return render(request, "TestHoax/ikutkegiatan.html", context)

def olahdata(request):
    url = "https://api.agify.io/?name=" + request.GET['name']
    ret = requests.get(url)
    hasil = json.loads(ret.content)
    return JsonResponse(hasil, safe=False)

def buatkegiatan(request):
    daftar = Kegiatan.objects.all()
    if request.method=="POST":
        form = FormKegiatan(request.POST)
        if form.is_valid():
            simpan = form.save()
            simpan.save()
            form = FormKegiatan()

    else:
        form = FormKegiatan()

    context = {'form':form, 'daftar':daftar}
    return render(request, "TestHoax/buatkegiatan.html", context)

def deletekegiatan(request, pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    context = {'kegiatan':kegiatan}
    if request.method=="POST":
        kegiatan.delete()
        return render(request, 'TestHoax/sudahhapuskegiatan.html', context)
    
    return render(request, 'TestHoax/deletekegiatan.html', context)

def daftarkegiatan(request):
    daftar = UserKegiatan.objects.all()
    context = {'List': daftar}
    return render(request, 'TestHoax/daftarkegiatan.html', context)

def cekdata(request):
    nama = request.GET.get('nama', none)
    kegiatan = request.GET.get('kegiatan', none)
    data = {
        'is_exist': Kegiatan.objects.filter(nama__iexact=nama).exists() and Kegiatan.objects.filter(kegiatan__ixact=kegiatan).exists()
    }

    