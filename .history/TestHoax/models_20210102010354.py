from django.db import models

# Create your models here.

class JawabanUser(models.Model):
    CHOICES=[('select1','Berita Benar'), ('select2','Hoaks')]
    Jawaban = models.CharField(max_length=7, choices=CHOICES, default="select1")
    Jawaban_ID = models.IntegerField()

class Kegiatan(models.Model):
    nama = models.CharField(max_length=50, null=True)

class UserKegiatan(models.Model):
    nama = models.CharField(max_length=50, null=True)
    kegiatan = models.ForeignKey(Kegiatan, null=True, on_delete=models.CASCADE)
    

    