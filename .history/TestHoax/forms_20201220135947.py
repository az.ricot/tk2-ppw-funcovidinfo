from django import forms
from .models import JawabanUser

class FormSoal(forms.ModelForm):

    class Meta:
        model = JawabanUser
        fields = ['Jawaban']
        
        widgets = {
               'Jawaban': forms.RadioSelect(attrs={'class': 'Test123'})
           }

        