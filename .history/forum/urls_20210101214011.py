from django.urls import path

from . import views

app_name = 'forum'

urlpatterns = [
    path('', views.forum, name='forum1'),
    path('category/<str:category>/', views.forum, name='forum2'),
    path('addPost/', views.editPost, name='addPost'),
    path('editPost/<int:pk>/', views.editPost, name='editPost'),
    path('posts/<int:pk>/', views.readPost, name='readPost'),
    path('editComment/<int:pk>/', views.editComment, name='editComment'),
    path('addReply/<int:pkC>/', views.editReply, name='addReply'),
    path('editReply/<int:pkC>/<int:pkR>/', views.editReply, name='editReply'),
    path('addReplyReply//<int:pkR>/', views.editReply, name='addReplyReply'),
    path('profil/<str:username>/', views.profil, name='profil'),
    path('profil/<str:username>/warnaRandom/', views.warnaRandom, name='warnaRandom'),
    path('confirmDeletePost/<int:pk>/', views.confirmDeletePost, name='confirmDeletePost'),
    path('deletePost/<int:pk>/', views.deletePost, name='deletePost'),
    path('confirmDeleteComment/<int:pk>/', views.confirmDeleteComment, name='confirmDeleteComment'),
    path('deleteComment/<int:pk>/', views.deleteComment, name='deleteComment'),
    path('confirmDeleteReply/<int:pk>/', views.confirmDeleteReply, name='confirmDeleteReply'),
    path('deleteReply/<int:pk>/', views.deleteReply, name='deleteReply')
]