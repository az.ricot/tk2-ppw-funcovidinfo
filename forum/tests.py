from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import forum, editPost, readPost, editComment, editReply, profil, warnaRandom, confirmDeletePost, deletePost, confirmDeleteComment, deleteComment, confirmDeleteReply, deleteReply
from .forms import PostForm, CommentForm, ReplyCommentForm, ColorForm
from .models import Post, Comment, ReplyComment, Color
from django.contrib.auth.models import User


class Test(TestCase):
    
    def test_url_tidak_logged_in_ada(self):
        response = Client().get('/forum/')
        self.assertEquals(response.status_code, 200)
        response = Client().get('/forum/', {'category':'umum'})
        self.assertEquals(response.status_code, 200)
        response = Client().get('/forum/addPost/')
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:editPost', kwargs={'pk':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:editComment', kwargs={'pk':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:addReply', kwargs={'pkC':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:editReply', kwargs={'pkC':1, 'pkR':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:addReplyReply', kwargs={'pkR':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:profil', kwargs={'username':'user'}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:confirmDeletePost', kwargs={'pk':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:confirmDeleteComment', kwargs={'pk':1}))
        self.assertEquals(response.status_code, 200)
        response = Client().get(reverse('forum:confirmDeleteReply', kwargs={'pk':1}))
        self.assertEquals(response.status_code, 200)
        
    def test_template_saat_tidak_login(self):
        response = Client().get('/forum/')
        self.assertTemplateUsed(response, 'forum/forum.html')
        response = Client().get(reverse('forum:forum2', kwargs={'category':'umum'}))
        self.assertTemplateUsed(response, 'forum/forum.html')
        response = Client().get(reverse('forum:forum2', kwargs={'category':'user'}))
        response = Client().get('/forum/addPost/')
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:editPost', kwargs={'pk':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:editComment', kwargs={'pk':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:addReply', kwargs={'pkC':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:editReply', kwargs={'pkC':1, 'pkR':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:addReplyReply', kwargs={'pkR':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:profil', kwargs={'username':'user'}))
        self.assertTemplateUsed(response, 'forum/profil.html')
        response = Client().get(reverse('forum:confirmDeletePost', kwargs={'pk':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:confirmDeleteComment', kwargs={'pk':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        response = Client().get(reverse('forum:confirmDeleteReply', kwargs={'pk':1}))
        self.assertTemplateUsed(response, 'forum/notLoggedIn.html')
        
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='user', password='abcd1234')
        
        self.client.login(username='user', password='abcd1234')
        user = User.objects.get(username='user')
        
        self.warna = Color(color1=1, color2=2, color3=3, user=user)
        self.warna.save()
        self.post = Post(judul='judul', kategori='umum', isi='isi', penulis=user, warna=self.warna, strings='judul '+user.username+' isi')
        self.post.save()
        self.komen = Comment(isi='isi', post=self.post, penulis=user, jumlahReply='0')
        self.komen.save()
        self.reply = ReplyComment(isi='isi', komen=self.komen, penulis=user)
        self.reply.save()
        
    
    def test_url_dan_template_forum_saat_login(self):
        response = self.client.get(reverse('forum:forum1'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/forum.html')
        self.assertEquals(response.context['category'], 'semuaDiskusi')
        
        response = self.client.get(reverse('forum:forum2', kwargs={'category':'user'}))
        response = self.client.get(reverse('forum:forum2', kwargs={'category':'info'}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/forum.html')
        self.assertEquals(response.context['category'], 'info')
        
    def test_url_dan_template_menambah_post(self):
        response = self.client.get(reverse('forum:addPost'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/editPost.html')
        
        Color.objects.all().delete() #untuk otomatis menambahkan warna pada user saat user belum punya warna
        
        response = self.client.post(reverse('forum:addPost'), {'judul':'judul', 'isi':'isi', 'penulis':self.user, 'kategori':'umum'})
        jumlah_post = Post.objects.all().count()
        postTest = Post.objects.last()
        self.assertEqual(postTest.judul, 'judul')
        self.assertEqual(postTest.isi, 'isi')
        self.assertEqual(jumlah_post, 2)
        
    def test_url_dan_template_edit_post(self):
        response = self.client.get(reverse('forum:editPost', kwargs={'pk':self.post.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/editPost.html')
    
        postTest = Post(judul='judul', isi='isi', penulis=self.user, kategori='umum')
        postTest.save()
        
        postEval = Post.objects.last()
        self.assertEqual(postEval.judul, 'judul')
    
        response = self.client.post(reverse('forum:editPost', kwargs={'pk':postEval.pk}), {'judul':'judul telah diedit', 'isi':'isi', 'kategori':'umum'})
        postEval2 = Post.objects.get(pk=postEval.pk)
        self.assertEqual(postEval2.judul, 'judul telah diedit')
    
    def test_url_dan_template_baca_post(self):
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/readPost.html')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn(self.post.judul, html_didapatkan)
        self.assertIn(self.post.isi, html_didapatkan)
        self.assertIn('Diskusi Umum', html_didapatkan)
        self.assertIn('Post Komentar', html_didapatkan)
    
    def test_url_dan_template_tambah_komen_pada_post(self):
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertNotIn('komen sayaaa', html_didapatkan)
        
        response = self.client.post(reverse('forum:readPost', kwargs={'pk':self.post.pk}), {'isi':'komen sayaaa'})
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertIn(self.user.username, html_didapatkan)
        self.assertIn('komen sayaaa', html_didapatkan)
    
    def test_url_dan_template_edit_komen_pada_post(self):
        response = self.client.get(reverse('forum:editComment', kwargs={'pk':self.komen.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/editComment.html')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn('isi', html_didapatkan)
        
        response = self.client.post(reverse('forum:editComment', kwargs={'pk':self.komen.pk}), {'isi':'isi telah diedit'})
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertIn('isi telah diedit', html_didapatkan)
    
    def test_url_dan_template_menambah_dan_mengedit_reply_pada_komen(self):
        #cek html dan url tambah reply
        response = self.client.get(reverse('forum:addReply', kwargs={'pkC':self.reply.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/editComment.html')
        
        #belum ada reply dengan string 'balasan dari komen'
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertNotIn('balasan dari komen', html_didapatkan)
        
        #lakukan penambahan dan pengecekan string 'balasan dari komen'
        response = self.client.post(reverse('forum:addReply', kwargs={'pkC':self.komen.pk}), {'isi':'balasan dari komen'})
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        replyBaru = ReplyComment.objects.last()
        self.assertEqual(replyBaru.komen, self.komen)
        html_didapatkan = response.content.decode('utf8')
        self.assertIn('balasan dari komen', html_didapatkan)
        
        #cek html dan url edit reply
        response = self.client.get(reverse('forum:editReply', kwargs={'pkC':self.komen.pk, 'pkR':replyBaru.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/editComment.html')
        
        #lakukan penambahan dan pengecekan string 'balasan dari komen'
        response = self.client.post(reverse('forum:editReply', kwargs={'pkC':self.komen.pk, 'pkR':replyBaru.pk}), {'isi':'balasan sudah diedit'})
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertNotIn('balasan dari komen', html_didapatkan)
        self.assertIn('balasan sudah diedit', html_didapatkan)
    
    def test_url_dan_template_menambah__reply_pada_reply(self):
        response = self.client.get(reverse('forum:addReplyReply', kwargs={'pkR':self.reply.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/editComment.html')
        
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertNotIn('balasan untuk balasan', html_didapatkan)
        
        response = self.client.post(reverse('forum:addReplyReply', kwargs={'pkR':self.reply.pk}), {'isi':'balasan untuk balasan'})
        response = self.client.get(reverse('forum:readPost', kwargs={'pk':self.post.pk}))
        html_didapatkan = response.content.decode('utf8')
        self.assertIn('balasan untuk balasan', html_didapatkan)
        replyToReply = ReplyComment.objects.last()
        self.assertEqual(replyToReply.targetBalasan, self.reply)
        
    def test_url_dan_template_melihat_profil(self):
        response = self.client.get(reverse('forum:profil', kwargs={'username':'user'}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/profil.html')
        html_didapatkan = response.content.decode('utf8')
        self.assertIn('Username : user', html_didapatkan)
        
    def test_url_dan_template_mengubah_warna_profil(self):
        Color.objects.all().delete()
        
        response = self.client.post(reverse('forum:profil', kwargs={'username':'user'}), {'color1':10, 'color2':20, 'color3':30})
        self.assertRedirects(response, expected_url=reverse('forum:profil', kwargs={'username':'user'}), status_code=302, target_status_code=200)
        warnaUser = Color.objects.get(user=self.user)
        self.assertEqual(warnaUser.color1, 10)
        self.assertEqual(warnaUser.color2, 20)
        self.assertEqual(warnaUser.color3, 30)
        
        color_sebelum_random = warnaUser
        response = self.client.get(reverse('forum:warnaRandom', kwargs={'username':'user'}))
        color_setelah_random = Color.objects.get(user=self.user)
        #karena random bisa saja tetap sama colornya
        if color_setelah_random.color1 != 10:
            self.assertNotEqual(color_sebelum_random.color1, color_setelah_random.color1)
        
        #jika username user tak ada
        response = self.client.get(reverse('forum:warnaRandom', kwargs={'username':'a'}))
        self.assertEquals(response.status_code, 403)
        
            
    def test_url_dan_template_menghapus_post(self):
        response = self.client.get(reverse('forum:confirmDeletePost', kwargs={'pk':self.post.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/confirmDelete.html')
        
        #buat post baru
        response = self.client.post(reverse('forum:addPost'), {'judul':'judul2', 'isi':'isi2', 'kategori':'th'})
        jumlah_post = Post.objects.all().count()
        self.assertEqual(jumlah_post, 2)
        post_lama = Post.objects.first()
        self.assertEqual(post_lama.judul, 'judul')
        
        #hapus post lama
        response = self.client.get(reverse('forum:deletePost', kwargs={'pk':self.post.pk}))
        jumlah_post = Post.objects.all().count()
        self.assertEqual(jumlah_post, 1)
        post_baru = Post.objects.last()
        self.assertEqual(post_baru.judul, 'judul2')
        
        #komen dan reply sudah tak ada karena post lama (post pertama) sudah dihapus
        jumlah_komen = Comment.objects.all().count()
        jumlah_reply = ReplyComment.objects.all().count()
        self.assertEqual(jumlah_komen, 0)
        self.assertEqual(jumlah_reply, 0)
        
    def test_url_dan_template_menghapus_komen(self):
        response = self.client.get(reverse('forum:confirmDeleteComment', kwargs={'pk':self.komen.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/confirmDelete.html')
        
        #buat komen baru
        response = self.client.post(reverse('forum:readPost', kwargs={'pk':self.post.pk}), {'isi':'isi komen baru'})
        jumlah_komen = Comment.objects.all().count()
        self.assertEqual(jumlah_komen, 2)
        komen_lama = Comment.objects.first()
        self.assertEqual(komen_lama.isi, 'isi')
        
        #hapus komen lama
        response = self.client.get(reverse('forum:deleteComment', kwargs={'pk':self.komen.pk}))
        jumlah_komen = Comment.objects.all().count()
        self.assertEqual(jumlah_komen, 1)
        komen_baru = Comment.objects.last()
        self.assertEqual(komen_baru.isi, 'isi komen baru')
        
        #reply sudah tak ada karena komen lama (komen pertama) sudah dihapus
        jumlah_reply = ReplyComment.objects.all().count()
        self.assertEqual(jumlah_reply, 0)
        
    def test_url_dan_template_menghapus_reply(self):
        response = self.client.get(reverse('forum:confirmDeleteReply', kwargs={'pk':self.reply.pk}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'forum/confirmDelete.html')
        
        #buat reply baru
        response = self.client.post(reverse('forum:addReply', kwargs={'pkC':self.komen.pk}), {'isi':'isi balasan baru'})
        jumlah_reply = ReplyComment.objects.all().count()
        self.assertEqual(jumlah_reply, 2)
        reply_lama = ReplyComment.objects.first()
        self.assertEqual(reply_lama.isi, 'isi')
        
        #hapus reply lama
        response = self.client.get(reverse('forum:deleteReply', kwargs={'pk':self.reply.pk}))
        jumlah_reply = ReplyComment.objects.all().count()
        self.assertEqual(jumlah_reply, 1)
        reply_baru = ReplyComment.objects.last()
        self.assertEqual(reply_baru.isi, 'isi balasan baru')
        
    def test_ajax(self):
        response = Client().get('/forum/loadMore/?c=1&q=semuaDiskusi')
        response = Client().get('/forum/category/umum/search/?c=1&q=semuaDiskusi')
        response = Client().get('/forum/search/?c=a&q=all')
        response = Client().get('/forum/category/umum/search/?c=a&q=all')
        response = Client().get('/forum/category/umum/search/?c=a&q=judul')
        response = Client().get('/forum/category/umum/search/?c=a&q=isi')
        response = Client().get('/forum/category/umum/search/?c=a&q=penulis')
        
        
        
        
