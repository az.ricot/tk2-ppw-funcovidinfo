# Generated by Django 3.1.4 on 2020-12-31 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0004_replycomment_targetbalasan'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='warna',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
