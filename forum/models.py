from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import datetime, date

KATEGORI = (
    ('', '*Pilih Kategori Diskusi*'),
    ('umum', 'Diskusi Umum'),
    ('info', 'Informasi Covid-19'),
    ('hd', 'Hoax Destroyer'),
    ('hs', 'Hoax Spreader'),
    ('th', 'Test Hoax'),
    ('tp', 'Test Pengetahuan Covid-19'),
)

class Post(models.Model):
    judul = models.CharField(max_length = 150)
    kategori = models.CharField(max_length = 25, choices = KATEGORI, default='')
    isi = models.TextField()
    penulis = models.ForeignKey(User, on_delete = models.CASCADE)
    tanggalPost = models.DateTimeField(default=timezone.now)
    warna = models.CharField(max_length = 20, null=True, blank=True)
    strings = models.TextField(null=True, blank=True)
    
class Comment(models.Model):
    isi = models.TextField()
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    penulis = models.ForeignKey(User, on_delete = models.CASCADE)
    tanggalPost = models.DateTimeField(default=timezone.now)
    jumlahReply = models.CharField(max_length = 100, null=True, blank=True)
    
class ReplyComment(models.Model):
    isi = models.TextField()
    penulis = models.ForeignKey(User, on_delete = models.CASCADE)
    komen = models.ForeignKey(Comment, on_delete = models.CASCADE)
    targetBalasan = models.ForeignKey('self', on_delete = models.CASCADE, null=True, blank=True)
    tanggalPost = models.DateTimeField(default=timezone.now)

class Color(models.Model):
    color1 = models.IntegerField()
    color2 = models.IntegerField()
    color3 = models.IntegerField()
    user = models.ForeignKey(User, on_delete = models.CASCADE)