from django.shortcuts import get_object_or_404, redirect, render, reverse
from django.http import HttpResponseForbidden, HttpResponseNotFound, HttpResponseRedirect, HttpResponse
from .forms import PostForm, CommentForm, ReplyCommentForm, ColorForm
from .models import Post, Comment, ReplyComment, Color
import random
from django.contrib.auth.models import User
from django.core import serializers

def forum(request, category=None):
    #warna
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
    semuaWarna = Color.objects.all()
    
    if category == 'user' and request.user.is_authenticated:
        diskusi = Post.objects.filter(penulis = request.user).order_by('-tanggalPost')[:10]
    elif category != 'semuaDiskusi' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-tanggalPost')[:10]
    else:
        diskusi = Post.objects.all().order_by('-tanggalPost')[:10]
        
    if (category == 'user' and not request.user.is_authenticated) or (category != 'semuaDiskusi' and category != 'umum' and category != 'info' and category != 'hd' and category != 'hs' and category != 'th' and category != 'tp' and category != 'user' and category != None):
        return HttpResponseNotFound()
    if category:
        context = {'category' : category, 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser}
        return render(request, 'forum/forum.html', context)
        
    context = {'category' : 'semuaDiskusi', 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser}
    return render(request, 'forum/forum.html', context)


def editPost(request, pk=None):
    
    if request.user.is_authenticated:
    
        if pk:
            post = get_object_or_404(Post, pk=pk)
            if post.penulis != request.user:
                return HttpResponseForbidden()
        else:
            post = Post(penulis = request.user)

        form = PostForm(request.POST or None, instance = post)
        if request.POST and form.is_valid():
            form.save()
            
            warna = Color.objects.filter(user = request.user)
            if not warna:
                warna = Color(
                    color1 = random.randint(0, 255),
                    color2 = random.randint(0, 255),
                    color3 = random.randint(0, 255),
                    user = request.user
                )
                warna.save()
            
            temp = Post.objects.last()
            tempWarna = Color.objects.get(user = temp.penulis)
            temp2Warna = str(tempWarna.color1) + " " + str(tempWarna.color2) + " " + str(tempWarna.color3)
            temp.warna = temp2Warna
            
            tempStrings = temp.judul + " " + temp.penulis.username + " " + temp.isi
            temp.strings = tempStrings
            temp.save()
            
            return redirect('forum:readPost', post.pk)
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        return render(request, 'forum/editPost.html', {
            'form': form,
            'warnaUser' : warnaUser
        })
    
    return render(request, 'forum/notLoggedIn.html', {'message':'"Mulai Diskusi"'})
   
   
def readPost(request, pk):
    diskusi = get_object_or_404(Post, pk = pk)
    komen = Comment.objects.filter(post = diskusi).order_by('tanggalPost')
    reply = ReplyComment.objects.all().order_by('tanggalPost')
        
    #pick random Posts
    if request.user.is_authenticated:
        postWithoutUser = Post.objects.exclude(penulis = request.user).exclude(kategori = diskusi.kategori).order_by('-tanggalPost')
    else:
        postWithoutUser = Post.objects.exclude(kategori = diskusi.kategori).order_by('-tanggalPost')
    if len(postWithoutUser) > 3:
        random_pk = random.randint(0, len(postWithoutUser)-3)
    else:
        random_pk = 0
    randomDiskusi = postWithoutUser[random_pk:random_pk+3]
        
    #pick latest posts in category
    if request.user.is_authenticated:
        postWithoutUser = Post.objects.filter(kategori = diskusi.kategori).exclude(penulis = request.user).exclude(judul = diskusi.judul).order_by('-tanggalPost')
    else:
        postWithoutUser = Post.objects.filter(kategori = diskusi.kategori).exclude(judul = diskusi.judul).order_by('-tanggalPost')
    diskusiLatestInCategory = postWithoutUser[0:3]
        
    #Form Comment
    if request.user.is_authenticated:
        komenForForm = Comment(penulis = request.user, post = diskusi)
        form = CommentForm(request.POST or None, instance = komenForForm)
        if request.POST and form.is_valid():
            form.save()
            
            kom = Comment.objects.last()
            kom.jumlahReply = str(0);
            kom.save()
            
            return HttpResponseRedirect(request.path_info)
    else:
        form = None
        
    #warna
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
    semuaWarna = Color.objects.all()
    return render(request, 'forum/readPost.html', {
        'diskusi' : diskusi, 
        'komen' : komen, 
        'reply' : reply, 
        'form' : form, 
        'randomDiskusi' : randomDiskusi, 
        'diskusiLatestInCategory' : diskusiLatestInCategory, 
        'semuaWarna' : semuaWarna, 
        'warnaUser' : warnaUser
        })
    

def editComment(request, pk):
    if request.user.is_authenticated:
    
        komen = get_object_or_404(Comment, pk=pk)
        if komen.penulis != request.user:
            return HttpResponseForbidden()
                
        form = CommentForm(request.POST or None, instance = komen)
        if request.POST and form.is_valid():
            form.save()
            return redirect('forum:readPost', komen.post.pk)
        
        type = 'edit'
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/editComment.html', {
            'form': form,
            'diskusi' : komen.post,
            'komen' : komen,
            'type' : type,
            'semuaWarna' : semuaWarna,
            'warnaUser' : warnaUser
        })
    return render(request, 'forum/notLoggedIn.html', {'message':'buat atau ubah komentar'})
    

def editReply(request, pkC = None, pkR = None):
    if request.user.is_authenticated:
        target = None
        
        if not pkC and pkR:
            target = get_object_or_404(ReplyComment, pk=pkR)
            komen = target.komen
        else:
            komen = get_object_or_404(Comment, pk=pkC)
    
        if pkC and pkR:
            reply = get_object_or_404(ReplyComment, pk=pkR)
            target = reply
            type = 'edit'
            if reply.penulis != request.user:
                return HttpResponseForbidden()
        elif not pkC and pkR:
            reply = ReplyComment(penulis = request.user, komen = komen, targetBalasan = target, isi = "@"+target.penulis.username+" ")
            type = 'add'
        else:
            reply = ReplyComment(penulis = request.user, komen = komen)
            type = 'add'

        form = ReplyCommentForm(request.POST or None, instance = reply)
        if request.POST and form.is_valid():
            form.save()
            if type == 'add':
                rep = ReplyComment.objects.last()
                kom = rep.komen
                komJumRep = int(kom.jumlahReply)
                kom.jumlahReply = str(komJumRep + 1)
                kom.save()
            return redirect('forum:readPost', komen.post.pk)
        
        #targetBalasan
        if target != None:
            listTarget = [target]
            while target.targetBalasan != None:
                target = target.targetBalasan
                listTarget.append(target)
            listTarget.reverse()
        else:
            listTarget = None
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        
        context = {
            'form': form,
            'komen' : komen,
            'listTarget' : listTarget,
            'type' : type,
            'semuaWarna' : semuaWarna,
            'warnaUser' : warnaUser
        }
        return render(request, 'forum/editComment.html', context)
    
    return render(request, 'forum/notLoggedIn.html', {'message':'buat atau ubah balasan'})


def profil(request, username):
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
        
    #check if user exist by username
    filterUser = User.objects.filter(username = username)
    
    if filterUser:
        user = User.objects.get(username = username)
        #check if user aldery have color or not
        filter = Color.objects.filter(user = user)
        
        diskusi = Post.objects.filter(penulis = user).order_by('-tanggalPost')
        semuaWarna = Color.objects.all()
        
        if filter:
            warna = Color.objects.get(user = user)
        
        if request.user == user:
            if not filter:
                warna = Color(user = user)
                
            form = ColorForm(request.POST or None, instance = warna)
            if request.POST and form.is_valid():
                form.save()
                
                posts = Post.objects.all()
                for post in posts:
                    tempWarna = Color.objects.get(user = post.penulis)
                    temp2Warna = str(tempWarna.color1) + " " + str(tempWarna.color2) + " " + str(tempWarna.color3)
                    post.warna = temp2Warna
                    post.save()

                return HttpResponseRedirect(request.path_info)
            
            return render(request, 'forum/profil.html', {'userInProfil' : user, 'warna' : warna, 'form' : form, 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
            
        if not filter:
            warna = None
        
        return render(request, 'forum/profil.html', {'userInProfil' : user, 'warna' : warna, 'diskusi' : diskusi, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    else:
        return HttpResponseNotFound()
        

def warnaRandom(request, username):
    if request.user.is_authenticated:
        if username == request.user.username:
            warna = Color.objects.filter(user = request.user)
            if not warna:
                warna = Color(
                    color1 = random.randint(0, 255),
                    color2 = random.randint(0, 255),
                    color3 = random.randint(0, 255),
                    user = request.user
                )
                warna.save()
            else:
                warna = Color.objects.get(user = request.user)
                warna.color1 = random.randint(0, 255)
                warna.color2 = random.randint(0, 255)
                warna.color3 = random.randint(0, 255)
                warna.save()
            
            posts = Post.objects.all()
            for post in posts:
                tempWarna = Color.objects.get(user = post.penulis)
                temp2Warna = str(tempWarna.color1) + " " + str(tempWarna.color2) + " " + str(tempWarna.color3)
                post.warna = temp2Warna
                post.save()
            
            return redirect('forum:profil', username)
        else:
            return HttpResponseForbidden()
    else:
        return render(request, 'forum/notLoggedIn.html', {'message':'"Warna Random"'})
    
    
def confirmDeletePost(request, pk):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, pk=pk)
        if post.penulis != request.user:
            return HttpResponseForbidden()
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {'diskusi':post, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus diskusi'})


def deletePost(request, pk):
    if request.user.is_authenticated:
        post = Post.objects.filter(pk=pk)
        if post[:1].get().penulis != request.user:
            return HttpResponseForbidden()
        post.delete()
        return redirect('forum:forum1')
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus diskusi'})
    
    
def confirmDeleteComment(request, pk):
    if request.user.is_authenticated:
        comment = get_object_or_404(Comment, pk=pk)
        if comment.penulis != request.user:
            return HttpResponseForbidden()
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {'komen':comment, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus komentar'})


def deleteComment(request, pk):
    if request.user.is_authenticated:
        komen = Comment.objects.filter(pk=pk)
        post = komen[:1].get().post
        if komen[:1].get().penulis != request.user:
            return HttpResponseForbidden()
        komen.delete()
        return redirect('forum:readPost', post.pk)
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus komentar'})
    
    
def confirmDeleteReply(request, pk):
    if request.user.is_authenticated:
        
        target = get_object_or_404(ReplyComment, pk=pk)
        reply = target
        komen = reply.komen
        if target.penulis != request.user:
            return HttpResponseForbidden()
        
        #targetBalasan
        if target != None:
            listTarget = [target]
            while target.targetBalasan != None:
                target = target.targetBalasan
                listTarget.append(target)
            listTarget.reverse()
        else:
            listTarget = None
        
        #warna
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
        semuaWarna = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {'reply':reply, 'komen':komen, 'listTarget':listTarget, 'semuaWarna' : semuaWarna, 'warnaUser' : warnaUser})
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus balasan'})

def deleteReply(request, pk):
    if request.user.is_authenticated:
        reply = ReplyComment.objects.filter(pk=pk)
        post = reply[:1].get().komen.post
        if reply[:1].get().penulis != request.user:
            return HttpResponseForbidden()
            
        kom = reply[:1].get().komen
        reply.delete()
        
        count = 0
        allRep = ReplyComment.objects.all()
        for r in allRep:
            if r.komen == kom:
                count += 1
        kom.jumlahReply = str(count);
        kom.save()
        
        return redirect('forum:readPost', post.pk)
    
    return render(request, 'forum/notLoggedIn.html', {'message':'hapus balasan'})
    
    
def loadMore(request, category = None):
    count = int(request.GET['c'])
    category = request.GET['q']
    counter0 = (count * 10) - 10
    counter1 = count * 10
    diskusi = None;
    
    if category == 'user':
        diskusi = Post.objects.filter(penulis = request.user).order_by('-tanggalPost')[counter0:counter1]
    elif category != 'semuaDiskusi' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-tanggalPost')[counter0:counter1]
    else:
        diskusi = Post.objects.all().order_by('-tanggalPost')[counter0:counter1]
    
    data = serializers.serialize('json', diskusi, use_natural_foreign_keys=True, use_natural_primary_keys=True)
    return HttpResponse(data, content_type='application/json')
    
    
def search(request, category = None):
    input = request.GET['c']
    selected = request.GET['q']
    listObj = []
    listInd = []
    
    if category == 'user':
        diskusi = Post.objects.filter(penulis = request.user).order_by('-tanggalPost')
    elif category != 'semuaDiskusi' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-tanggalPost')
    else:
        diskusi = Post.objects.all().order_by('-tanggalPost')
    
    temp = 0
    if selected == 'all':
        for disk in diskusi:
            temp = searchWord(disk.strings.lower(), input.lower())
            if temp != -1:
                listObj.append(disk)
                listInd.append(temp)
    elif selected == 'judul':
        for disk in diskusi:
            temp = searchWord(disk.judul.lower(), input.lower())
            if temp != -1:
                listObj.append(disk)
                listInd.append(temp)
    elif selected == 'penulis':
        for disk in diskusi:
            temp = searchWord(disk.penulis.username.lower(), input.lower())
            if temp != -1:
                listObj.append(disk)
                listInd.append(temp)
    elif selected == 'isi':
        for disk in diskusi:
            temp = searchWord(disk.isi.lower(), input.lower())
            if temp != -1:
                listObj.append(disk)
                listInd.append(temp)
    
    zipped_lists = list(zip(listInd, listObj))
    result1 = sorted(zipped_lists, key=lambda obj : obj[0], reverse=True)
    result = []
    
    for res in result1:
        result.append(res[1])
    
    data = serializers.serialize('json', result, use_natural_foreign_keys=True, use_natural_primary_keys=True)
    return HttpResponse(data, content_type='application/json')
    
    
def searchWord(text, word):
    index = 0
    counter = -1
    while index < len(text):
        index = text.find(word, index)
        if index == -1:
            break
        index += len(word)
        counter += 1
    return counter
    