
$(document).ready(function() {
    
    var pathname = window.location.pathname;
    var param = pathname.slice(-6, -1);
    var category;
    var tulisanCategory;
            
    if (param == 'skusi' || param == 'forum') {category = 'semuaDiskusi'; tulisanCategory = 'Semua Diskusi';} 
    else if (param == '/user') {category = 'user'; tulisanCategory = 'Diskusi Saya';}
    else if (param == '/umum') {category = 'umum'; tulisanCategory = 'Semua Diskusi';} 
    else if (param == '/info') {category = 'info'; tulisanCategory = 'Informasi Covid-19';} 
    else if (param == 'ry/hd') {category = 'hd'; tulisanCategory = 'Hoax Destroyer';} 
    else if (param == 'ry/hs') {category = 'hs'; tulisanCategory = 'Hoax Spreader';} 
    else if (param == 'ry/th') {category = 'th'; tulisanCategory = 'Test Hoax';} 
    else if (param == 'ry/tp') {category = 'tp'; tulisanCategory = 'Test Pengetahuan Covid-19';}
    
    $(".drpdwnSearch-content").append(
        "<div id='searchStart'>" +
            "Apa yang Anda ingin cari?" +
            "<br> Anda sedang mencari pada bagian <strong>'" + tulisanCategory + "'</strong>." +
        "</div>"
    );
    
    $('#load').hide();
    $('#load2').hide();
    $(".drpdwnSearch-content").hide();
    
    var count = 1;
    var scrollLoad = true;
    
    $(window).scroll(function() {
        if(scrollLoad && ($(document).height() - $(window).height())-$(window).scrollTop()<=1){
            scrollLoad = false
            $('#load').show();
            
            count = count + 1;
            
            $.ajax({
                url : 'loadMore/?c=' + count + '&q=' + category,
                success : function(data) {
                    
                    for (i = 0; i < data.length; i++) {
                        var classKategori;
                        var tulisanKategori;
                        
                        if (data[i].fields.kategori == 'umum') {classKategori = 'blue'; tulisanKategori = '(Diskusi Umum)';}
                        else if (data[i].fields.kategori == 'info') {classKategori = 'red'; tulisanKategori = '(Informasi Covid-19)';}
                        else if (data[i].fields.kategori == 'hd') {classKategori = 'yellow'; tulisanKategori = '(Hoax Destroyer)';}
                        else if (data[i].fields.kategori == 'hs') {classKategori = 'purple'; tulisanKategori = '(Hoax Spreader)';}
                        else if (data[i].fields.kategori == 'th') {classKategori = 'green'; tulisanKategori = '(Test Hoax)';}
                        else if (data[i].fields.kategori == 'tp') {classKategori = 'black'; tulisanKategori = '(Test Pengetahuan Covid-19)';}
                        
                        var warnas = data[i].fields.warna.split(" ");
                        
                        var loca = "location.href='/forum/posts/" + data[i].pk + "/';"
              
                        var res = "<div onclick=" + loca + " class='post " + data[i].fields.kategori + "'>" +
                                "<div class='kanankiri inside-post'>" +
                                    "<div class='profile-container'>" +
                                        "<div class='profile' style='background-color: rgb(" + warnas[0] + ", " + warnas[1] + ", " + warnas[2] + ");'>" + 
                                            "<div class='atasbawah center inisial'>" +
                                                data[i].fields.penulis[0].charAt(0).toUpperCase() +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                    "<div class='atasbawah alignatasbawahkiri snippet'>" +
                                        "<div class='judulPost'>" +
                                            data[i].fields.judul.slice(0,110) + "... <br>" +
                                        "</div>" +
                                        "<div class='subJudulPost'>" +
                                            "<a href='/forum/category/" + data[i].fields.kategori + "/' class='" + classKategori + "'>" +
                                                tulisanKategori +
                                            "</a> oleh " + 
                                            "<a href='/forum/profil/" + data[i].fields.penulis[0] + "' class='edit'>" + data[i].fields.penulis + "</a>" +
                                             " pada " + data[i].fields.tanggalPost + " WIB";
                        
                        
                        if (data[i].fields.penulis[0] == userNow) {
                            res = res + " | " + "<a href='/forum/editPost/" + data[i].pk + "' class='edit'> edit </a> | " +
                                "<a href='/forum/confirmDeletePost/" + data[i].pk + "' class='edit'> delete </a>";
                        }
                        
                                                           
                            res = res + "</div>" +
                                        "<div>" +
                                            data[i].fields.isi.slice(0,215) + "..." +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                            "</div>";
                        
                        $("#diskusi").append(res);
                    }
                    
                    if (data[9]) {
                        scrollLoad = true;
                    }
                    $('#load').hide();
                }
            });
        }
    });
    
    $("#inputSearch").focusin(function(){
        $(".drpdwnSearch-content").fadeIn(250);
    });
    
    $("#inputSearch").focusout(function(){
        $(".drpdwnSearch-content").fadeOut(250);
    });
    
    var temp1;
    var fired = false;

    $('#inputSearch').onkeydown = function() {
        if(!fired) {
            fired = true;
            temp1 = $('#inputSearch').val();
        }
    };
    $('#inputSearch').onkeyup = function() {
        fired = false;
    };
    
    var globalTimeout = null;  
    $('#inputSearch').keyup(function(e){
        var temp2 = $('#inputSearch').val();
        if (temp1 == temp2) {
             return;
        }
        
        if((e.keyCode == 8 || e.keyCode == 46) && !($("#inputSearch").val())) {
            $(".drpdwnSearch-content").empty();
            $('#load2').hide();
            $(".drpdwnSearch-content").append(
                "<div id='searchStart'>" +
                    "Apa yang Anda ingin cari?" +
                    "<br> Anda sedang mencari pada bagian <strong>'" + tulisanCategory + "'</strong>." +
                "</div>"
            );
        } else {
            $(".drpdwnSearch-content").empty();
            $('#load2').show();
        }
        
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);  
        }
        globalTimeout = setTimeout(searchFunct, 500);  
    });
    
    $('#select').change(function() {
        if ($("#inputSearch").val()) {
            $(".drpdwnSearch-content").empty();        
            $('#load2').show();
            searchFunct();    
        }
    });
    
    function searchFunct() {
        globalTimeout = null;
        
        var input = $("#inputSearch").val();
        var selected = $('#select').val();
        console.log(input);
        
        $.ajax({
            url : 'search/?c=' + input + '&q=' + selected,
            success : function(data) {
                $(".drpdwnSearch-content").empty();
                console.log(data);
                
                $(".drpdwnSearch-content").append("<br>Anda sedang mencari pada bagian <strong>" + tulisanCategory + "</strong><br>");
                
                for (i = 0; i < data.length; i++) {
                    var classKategori;
                    var tulisanKategori;
                        
                    if (data[i].fields.kategori == 'umum') {classKategori = 'blue'; tulisanKategori = '(Diskusi Umum)';}
                    else if (data[i].fields.kategori == 'info') {classKategori = 'red'; tulisanKategori = '(Informasi Covid-19)';}
                    else if (data[i].fields.kategori == 'hd') {classKategori = 'yellow'; tulisanKategori = '(Hoax Destroyer)';}
                    else if (data[i].fields.kategori == 'hs') {classKategori = 'purple'; tulisanKategori = '(Hoax Spreader)';}
                    else if (data[i].fields.kategori == 'th') {classKategori = 'green'; tulisanKategori = '(Test Hoax)';}
                    else if (data[i].fields.kategori == 'tp') {classKategori = 'black'; tulisanKategori = '(Test Pengetahuan Covid-19)';}
                       
                    var warnas = data[i].fields.warna.split(" ");
                        
                    var loca = "location.href='/forum/posts/" + data[i].pk + "/';"
              
                    var res = "<div onclick=" + loca + " class='post " + data[i].fields.kategori + "' " + "id='searchDivs'>" +
                            "<div class='kanankiri inside-post'>" +
                                "<div class='profile-container'>" +
                                    "<div class='profile' style='background-color: rgb(" + warnas[0] + ", " + warnas[1] + ", " + warnas[2] + ");'>" + 
                                        "<div class='atasbawah center inisial'>" +
                                            data[i].fields.penulis[0].charAt(0).toUpperCase() +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='atasbawah alignatasbawahkiri snippet'>" +
                                    "<div class='judulPost'>" +
                                        data[i].fields.judul.slice(0,110) + "... <br>" +
                                    "</div>" +
                                    "<div class='subJudulPost'>" +
                                        "<a href='/forum/category/" + data[i].fields.kategori + "/' class='" + classKategori + "'>" +
                                            tulisanKategori +
                                        "</a> oleh " + 
                                        "<a href='/forum/profil/" + data[i].fields.penulis[0] + "' class='edit'>" + data[i].fields.penulis + "</a>" +
                                         " pada " + data[i].fields.tanggalPost + " WIB";
                    
                    
                    if (data[i].fields.penulis[0] == userNow) {
                        res = res + " | " + "<a href='/forum/editPost/" + data[i].pk + "' class='edit'> edit </a> | " +
                            "<a href='/forum/confirmDeletePost/" + data[i].pk + "' class='edit'> delete </a>";
                    }
                        
                                                           
                        res = res + "</div>" +
                                    "<div>" +
                                        data[i].fields.isi.slice(0,215) + "..." +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";
                     
                    $(".drpdwnSearch-content").append(res);
                }
                
                $(".drpdwnSearch-content").append("<br>");
                $('#load2').hide();
                
                if (data.length < 1) {
                    $(".drpdwnSearch-content").empty();
                    $(".drpdwnSearch-content").append(
                        "<div id='searchStart'>" +
                            "Maaf, kata kunci <strong>'" + input + "'</strong> tidak menemukan diskusi yang sesuai." +
                            "<br> Anda sedang mencari pada bagian <strong>'" + tulisanCategory + "'</strong>." +
                        "</div>"
                    );
                }
            }
        });
    }
    
});