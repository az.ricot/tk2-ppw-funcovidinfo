from django.shortcuts import render, redirect
from django.views import generic
from .forms import AccountCreationForm , AccountLoginForm
from django.urls import reverse_lazy

class Signup(generic.CreateView):
    form_class = AccountCreationForm
    template_name = 'registration/signup.html'
    success_url = reverse_lazy('userAuth:login')