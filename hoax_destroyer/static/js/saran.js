const alertBox = document.getElementById('alert-box')
const form = document.getElementById('p-form')

const pertanyaan = document.getElementById('id_pertanyaan')
const opsi1 = document.getElementById('id_opsi1')
const opsi2 = document.getElementById('id_opsi2')
const opsi3 = document.getElementById('id_opsi3')
const opsi4 = document.getElementById('id_opsi4')

const csrf = document.getElementsByName('csrfmiddlewaretoken')
console.log(csrf)

const url = ""

const handleAlerts = (type, text) =>{
    if(type == "success"){
        alertBox.innerHTML = `<div class="alert alert-${type}" role="alert" style="background-color: #95B46A; margin: 10px;">
        ${text}
    </div>`
    }
    else{
        alertBox.innerHTML = `<div class="alert alert-${type}" role="alert" style="background-color: #EF2D56; margin: 10px">
        ${text}
    </div>`
    }
}

form.addEventListener('submit', e=>{
    e.preventDefault()

    const fd = new FormData()
    fd.append('csrfmiddlewaretoken', csrf[0].value)
    fd.append('pertanyaan', pertanyaan.value)
    fd.append('opsi1', opsi1.value)
    fd.append('opsi2', opsi2.value)
    fd.append('opsi3', opsi3.value)
    fd.append('opsi4', opsi4.value)

    $.ajax({
        type: 'POST',
        url: url,
        enctype: 'multipart/form-data',
        data: fd,
        success: function(response){
            console.log(response)
            const sText = `successfully saved`
            handleAlerts('success', sText)
            setTimeout(()=>{
                alertBox.innerHTML = ""
                pertanyaan.value = ""
                opsi1.value = ""
                opsi2.value = ""
                opsi3.value = ""
                opsi4.value = ""
            }, 3000)
        },
        error: function(error){
            console.log(error)
            handleAlerts('danger', 'ups..something went wrong')
        },
        cache: false,
        contentType: false,
        processData: false,
    })
})

console.log(form)