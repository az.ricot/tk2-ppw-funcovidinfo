# Generated by Django 3.1.3 on 2020-11-20 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hoax_destroyer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Saran',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pertanyaan', models.CharField(max_length=200)),
                ('opsi1', models.CharField(max_length=200)),
                ('opsi2', models.CharField(max_length=200)),
                ('opsi3', models.CharField(max_length=200)),
                ('opsi4', models.CharField(max_length=200)),
            ],
        ),
    ]
