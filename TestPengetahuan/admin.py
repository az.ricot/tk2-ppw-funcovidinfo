from django.contrib import admin
from .models import Pertanyaan,Choice,Nilai,NilaiSimpan
# Register your models here.

admin.site.register(Pertanyaan)
admin.site.register(Choice)
admin.site.register(Nilai)
admin.site.register(NilaiSimpan)