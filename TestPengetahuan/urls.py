from django.urls import path

from . import views

app_name = 'TestPengetahuan'

urlpatterns = [
    path('home/', views.home, name='home'),  
    path('ganti/', views.ganti, name='ganti'),  
    path('gantiajax/', views.gantiajax, name='gantiajax'), 
    path('soal1s/',views.soal1s,name='soal1s'),
    path('soal1j/<int:qid>/<int:wid>',views.soal1j,name='soal1j'),
    path('soal2s/<int:wid>/',views.soal2s,name='soal2s'),
    path('soal2j/<int:qid>/<int:wid>/',views.soal2j,name='soal2j'),
    path('soal3s/<int:wid>/',views.soal3s,name='soal3s'),
    path('soal3j/<int:qid>/<int:wid>/',views.soal3j,name='soal3j'),
    path('soal4s/<int:wid>/',views.soal4s,name='soal4s'),
    path('soal4j/<int:qid>/<int:wid>/',views.soal4j,name='soal4j'),
    path('soal5s/<int:wid>/',views.soal5s,name='soal5s'),
    path('soal5j/<int:qid>/<int:wid>/',views.soal5j,name='soal5j'),
    path('soal6s/<int:wid>/',views.soal6s,name='soal6s'),
    path('soal6j/<int:qid>/<int:wid>/',views.soal6j,name='soal6j'),
    path('soal7s/<int:wid>/',views.soal7s,name='soal7s'),
    path('soal7j/<int:qid>/<int:wid>/',views.soal7j,name='soal7j'),
    path('soal8s/<int:wid>/',views.soal8s,name='soal8s'),
    path('soal8j/<int:qid>/<int:wid>/',views.soal8j,name='soal8j'),
    path('soal9s/<int:wid>',views.soal9s,name='soal9s'),
    path('soal9j/<int:qid>/<int:wid>/',views.soal9j,name='soal9j'),
    path('soal10s/<int:wid>/',views.soal10s,name='soal10s'),
    path('soal10j/<int:qid>/<int:wid>/',views.soal10j,name='soal10j'),
    path('hasil/<int:wid>/',views.hasil,name='hasil'),
    path('leaderboard/',views.score),
]
