from django.test import TestCase, Client		
from django.urls import resolve,reverse
from .forms import gantiform
from .models import Pertanyaan,Choice,Nilai
from .views import home,soal1s,soal1j,soal2s,soal2j,soal3s,soal3j,soal4s,soal4j,soal5s,soal5j,soal6s,soal6j,soal7s,soal7j,soal8s,soal8j,soal9s,soal9j,soal10s,soal10j,hasil,ganti,score
from django.contrib.auth.models import User
from django.http import HttpRequest, JsonResponse, HttpResponse

class TestPengetahuanTest(TestCase):
    def test_home(self):
    	Pertanyaan.objects.all().delete()
    	Choice.objects.all().delete()
    	response = Client().get('/TestPengetahuan/home/')
    	self.assertEqual(response.status_code,200)

    def test_soal1s(self):
    	a11=Choice.objects.create(pertanyaan="a1",choice="#",benarsalah=False)
    	b11=Choice.objects.create(pertanyaan="b1",choice="#",benarsalah=False)
    	c11=Choice.objects.create(pertanyaan="c1",choice="#",benarsalah=True)
    	d11=Choice.objects.create(pertanyaan="d1",choice="#",benarsalah=False)
    	Soal1=Pertanyaan.objects.create(no=1,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal1s/')
    	self.assertEqual(response.status_code,200)

    def test_soal1j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a11=Choice.objects.create(pertanyaan="a1",choice="#",benarsalah=False)
    	b11=Choice.objects.create(pertanyaan="b1",choice="#",benarsalah=False)
    	c11=Choice.objects.create(pertanyaan="c1",choice="#",benarsalah=True)
    	d11=Choice.objects.create(pertanyaan="d1",choice="#",benarsalah=False)
    	Soal1=Pertanyaan.objects.create(no=1,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal1j/3/1')
    	self.assertEqual(response.status_code,200)

    def test_soal2s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a22=Choice.objects.create(pertanyaan="a2",choice="#",benarsalah=False)
    	b22=Choice.objects.create(pertanyaan="b2",choice="#",benarsalah=False)
    	c22=Choice.objects.create(pertanyaan="c2",choice="#",benarsalah=True)
    	d22=Choice.objects.create(pertanyaan="d2",choice="#",benarsalah=False)
    	Soal2=Pertanyaan.objects.create(no=2,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal2s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal2j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a22=Choice.objects.create(pertanyaan="a2",choice="#",benarsalah=False)
    	b22=Choice.objects.create(pertanyaan="b2",choice="#",benarsalah=False)
    	c22=Choice.objects.create(pertanyaan="c2",choice="#",benarsalah=True)
    	d22=Choice.objects.create(pertanyaan="d2",choice="#",benarsalah=False)
    	Soal2=Pertanyaan.objects.create(no=2,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal2j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal3s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a33=Choice.objects.create(pertanyaan="a3",choice="#",benarsalah=False)
    	b33=Choice.objects.create(pertanyaan="b3",choice="#",benarsalah=False)
    	c33=Choice.objects.create(pertanyaan="c3",choice="#",benarsalah=True)
    	d33=Choice.objects.create(pertanyaan="d3",choice="#",benarsalah=False)
    	Soal3=Pertanyaan.objects.create(no=3,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal3s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal3j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a33=Choice.objects.create(pertanyaan="a3",choice="#",benarsalah=False)
    	b33=Choice.objects.create(pertanyaan="b3",choice="#",benarsalah=False)
    	c33=Choice.objects.create(pertanyaan="c3",choice="#",benarsalah=True)
    	d33=Choice.objects.create(pertanyaan="d3",choice="#",benarsalah=False)
    	Soal3=Pertanyaan.objects.create(no=3,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal3j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal4s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a44=Choice.objects.create(pertanyaan="a4",choice="#",benarsalah=False)
    	b44=Choice.objects.create(pertanyaan="b4",choice="#",benarsalah=False)
    	c44=Choice.objects.create(pertanyaan="c4",choice="#",benarsalah=True)
    	d44=Choice.objects.create(pertanyaan="d4",choice="#",benarsalah=False)
    	Soal4=Pertanyaan.objects.create(no=4,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal4s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal4j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a44=Choice.objects.create(pertanyaan="a4",choice="#",benarsalah=False)
    	b44=Choice.objects.create(pertanyaan="b4",choice="#",benarsalah=False)
    	c44=Choice.objects.create(pertanyaan="c4",choice="#",benarsalah=True)
    	d44=Choice.objects.create(pertanyaan="d4",choice="#",benarsalah=False)
    	Soal4=Pertanyaan.objects.create(no=4,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal4j/3/1/')

    def test_soal5s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a55=Choice.objects.create(pertanyaan="a5",choice="#",benarsalah=False)
    	b55=Choice.objects.create(pertanyaan="b5",choice="#",benarsalah=False)
    	c55=Choice.objects.create(pertanyaan="c5",choice="#",benarsalah=True)
    	d55=Choice.objects.create(pertanyaan="d5",choice="#",benarsalah=False)
    	Soal5=Pertanyaan.objects.create(no=5,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal5s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal5j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a55=Choice.objects.create(pertanyaan="a5",choice="#",benarsalah=False)
    	b55=Choice.objects.create(pertanyaan="b5",choice="#",benarsalah=False)
    	c55=Choice.objects.create(pertanyaan="c5",choice="#",benarsalah=True)
    	d55=Choice.objects.create(pertanyaan="d5",choice="#",benarsalah=False)
    	Soal5=Pertanyaan.objects.create(no=5,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal5j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal6s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a55=Choice.objects.create(pertanyaan="a6",choice="#",benarsalah=False)
    	b55=Choice.objects.create(pertanyaan="b6",choice="#",benarsalah=False)
    	c55=Choice.objects.create(pertanyaan="c6",choice="#",benarsalah=True)
    	d55=Choice.objects.create(pertanyaan="d6",choice="#",benarsalah=False)
    	Soal6=Pertanyaan.objects.create(no=6,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal6s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal6j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a66=Choice.objects.create(pertanyaan="a6",choice="#",benarsalah=False)
    	b66=Choice.objects.create(pertanyaan="b6",choice="#",benarsalah=False)
    	c66=Choice.objects.create(pertanyaan="c6",choice="#",benarsalah=True)
    	d66=Choice.objects.create(pertanyaan="d6",choice="#",benarsalah=False)
    	Soal6=Pertanyaan.objects.create(no=6,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal6j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal7s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a77=Choice.objects.create(pertanyaan="a7",choice="#",benarsalah=False)
    	b77=Choice.objects.create(pertanyaan="b7",choice="#",benarsalah=False)
    	c77=Choice.objects.create(pertanyaan="c7",choice="#",benarsalah=True)
    	d77=Choice.objects.create(pertanyaan="d7",choice="#",benarsalah=False)
    	Soal7=Pertanyaan.objects.create(no=7,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal7s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal7j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a66=Choice.objects.create(pertanyaan="a7",choice="#",benarsalah=False)
    	b66=Choice.objects.create(pertanyaan="b7",choice="#",benarsalah=False)
    	c66=Choice.objects.create(pertanyaan="c7",choice="#",benarsalah=True)
    	d66=Choice.objects.create(pertanyaan="d7",choice="#",benarsalah=False)
    	Soal7=Pertanyaan.objects.create(no=7,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal7j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal8s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a88=Choice.objects.create(pertanyaan="a8",choice="#",benarsalah=False)
    	b88=Choice.objects.create(pertanyaan="b8",choice="#",benarsalah=False)
    	c88=Choice.objects.create(pertanyaan="c8",choice="#",benarsalah=True)
    	d88=Choice.objects.create(pertanyaan="d8",choice="#",benarsalah=False)
    	Soal8=Pertanyaan.objects.create(no=8,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal8s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal8j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a88=Choice.objects.create(pertanyaan="a8",choice="#",benarsalah=False)
    	b88=Choice.objects.create(pertanyaan="b8",choice="#",benarsalah=False)
    	c88=Choice.objects.create(pertanyaan="c8",choice="#",benarsalah=True)
    	d88=Choice.objects.create(pertanyaan="d8",choice="#",benarsalah=False)
    	Soal8=Pertanyaan.objects.create(no=8,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal8j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal9s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a99=Choice.objects.create(pertanyaan="a9",choice="#",benarsalah=False)
    	b99=Choice.objects.create(pertanyaan="b9",choice="#",benarsalah=False)
    	c99=Choice.objects.create(pertanyaan="c9",choice="#",benarsalah=True)
    	d99=Choice.objects.create(pertanyaan="d9",choice="#",benarsalah=False)
    	Soal9=Pertanyaan.objects.create(no=9,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal9s/1')
    	self.assertEqual(response.status_code,200)

    def test_soal9j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a99=Choice.objects.create(pertanyaan="a9",choice="#",benarsalah=False)
    	b99=Choice.objects.create(pertanyaan="b9",choice="#",benarsalah=False)
    	c99=Choice.objects.create(pertanyaan="c9",choice="#",benarsalah=True)
    	d99=Choice.objects.create(pertanyaan="d9",choice="#",benarsalah=False)
    	Soal9=Pertanyaan.objects.create(no=9,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal9j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal10s(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a100=Choice.objects.create(pertanyaan="a10",choice="#",benarsalah=False)
    	b100=Choice.objects.create(pertanyaan="b10",choice="#",benarsalah=False)
    	c100=Choice.objects.create(pertanyaan="c10",choice="#",benarsalah=True)
    	d100=Choice.objects.create(pertanyaan="d10",choice="#",benarsalah=False)
    	Soal10=Pertanyaan.objects.create(no=10,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal10s/1/')
    	self.assertEqual(response.status_code,200)

    def test_soal10j(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a99=Choice.objects.create(pertanyaan="a10",choice="#",benarsalah=False)
    	b99=Choice.objects.create(pertanyaan="b10",choice="#",benarsalah=False)
    	c99=Choice.objects.create(pertanyaan="c10",choice="#",benarsalah=True)
    	d99=Choice.objects.create(pertanyaan="d10",choice="#",benarsalah=False)
    	Soal9=Pertanyaan.objects.create(no=10,pertanyaan="#")
    	response = Client().get('/TestPengetahuan/soal10j/3/1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil_lebih_dari_100(self):
    	nilai = Nilai.objects.create(nilai=110,nama="Guest")
    	response = Client().get('/TestPengetahuan/hasil/1/')
    	self.assertEqual(response.status_code,200)

    def test_hasil_kurang_dari_100(self):
    	nilai = Nilai.objects.create(nilai=10,nama="Guest")
    	response = Client().get('/TestPengetahuan/hasil/1/')
    	self.assertEqual(response.status_code,200)

    def test_home_func(self):
        found = resolve('/TestPengetahuan/home/')
        self.assertEqual(found.func,home)

    def test_soal1s_func(self):
    	a11=Choice.objects.create(pertanyaan="a1",choice="#",benarsalah=False)
    	b11=Choice.objects.create(pertanyaan="b1",choice="#",benarsalah=False)
    	c11=Choice.objects.create(pertanyaan="c1",choice="#",benarsalah=True)
    	d11=Choice.objects.create(pertanyaan="d1",choice="#",benarsalah=False)
    	Soal1=Pertanyaan.objects.create(no=1,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal1s/')
    	self.assertEqual(found.func,soal1s)

    def test_soal1j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a11=Choice.objects.create(pertanyaan="a1",choice="#",benarsalah=False)
    	b11=Choice.objects.create(pertanyaan="b1",choice="#",benarsalah=False)
    	c11=Choice.objects.create(pertanyaan="c1",choice="#",benarsalah=True)
    	d11=Choice.objects.create(pertanyaan="d1",choice="#",benarsalah=False)
    	Soal1=Pertanyaan.objects.create(no=1,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal1j/1/1')
    	self.assertEqual(found.func,soal1j)

    def test_soal2s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a22=Choice.objects.create(pertanyaan="a2",choice="#",benarsalah=False)
    	b22=Choice.objects.create(pertanyaan="b2",choice="#",benarsalah=False)
    	c22=Choice.objects.create(pertanyaan="c2",choice="#",benarsalah=True)
    	d22=Choice.objects.create(pertanyaan="d2",choice="#",benarsalah=False)
    	Soal2=Pertanyaan.objects.create(no=2,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal2s/1/')
    	self.assertEqual(found.func,soal2s)

    def test_soal2j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a22=Choice.objects.create(pertanyaan="a2",choice="#",benarsalah=False)
    	b22=Choice.objects.create(pertanyaan="b2",choice="#",benarsalah=False)
    	c22=Choice.objects.create(pertanyaan="c2",choice="#",benarsalah=True)
    	d22=Choice.objects.create(pertanyaan="d2",choice="#",benarsalah=False)
    	Soal2=Pertanyaan.objects.create(no=2,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal2j/1/1/')
    	self.assertEqual(found.func,soal2j)

    def test_soal3s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a33=Choice.objects.create(pertanyaan="a3",choice="#",benarsalah=False)
    	b33=Choice.objects.create(pertanyaan="b3",choice="#",benarsalah=False)
    	c33=Choice.objects.create(pertanyaan="c3",choice="#",benarsalah=True)
    	d33=Choice.objects.create(pertanyaan="d3",choice="#",benarsalah=False)
    	Soal3=Pertanyaan.objects.create(no=3,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal3s/1/')
    	self.assertEqual(found.func,soal3s)

    def test_soal3j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a33=Choice.objects.create(pertanyaan="a3",choice="#",benarsalah=False)
    	b33=Choice.objects.create(pertanyaan="b3",choice="#",benarsalah=False)
    	c33=Choice.objects.create(pertanyaan="c3",choice="#",benarsalah=True)
    	d33=Choice.objects.create(pertanyaan="d3",choice="#",benarsalah=False)
    	Soal3=Pertanyaan.objects.create(no=3,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal3j/1/1/')
    	self.assertEqual(found.func,soal3j)

    def test_soal4s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a44=Choice.objects.create(pertanyaan="a4",choice="#",benarsalah=False)
    	b44=Choice.objects.create(pertanyaan="b4",choice="#",benarsalah=False)
    	c44=Choice.objects.create(pertanyaan="c4",choice="#",benarsalah=True)
    	d44=Choice.objects.create(pertanyaan="d4",choice="#",benarsalah=False)
    	Soal4=Pertanyaan.objects.create(no=4,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal4s/1/')
    	self.assertEqual(found.func,soal4s)

    def test_soal4j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a44=Choice.objects.create(pertanyaan="a4",choice="#",benarsalah=False)
    	b44=Choice.objects.create(pertanyaan="b4",choice="#",benarsalah=False)
    	c44=Choice.objects.create(pertanyaan="c4",choice="#",benarsalah=True)
    	d44=Choice.objects.create(pertanyaan="d4",choice="#",benarsalah=False)
    	Soal4=Pertanyaan.objects.create(no=4,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal4j/1/1/')
    	self.assertEqual(found.func,soal4j)

    def test_soal5s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a55=Choice.objects.create(pertanyaan="a5",choice="#",benarsalah=False)
    	b55=Choice.objects.create(pertanyaan="b5",choice="#",benarsalah=False)
    	c55=Choice.objects.create(pertanyaan="c5",choice="#",benarsalah=True)
    	d55=Choice.objects.create(pertanyaan="d5",choice="#",benarsalah=False)
    	Soal5=Pertanyaan.objects.create(no=5,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal5s/1/')
    	self.assertEqual(found.func,soal5s)

    def test_soal5j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a55=Choice.objects.create(pertanyaan="a5",choice="#",benarsalah=False)
    	b55=Choice.objects.create(pertanyaan="b5",choice="#",benarsalah=False)
    	c55=Choice.objects.create(pertanyaan="c5",choice="#",benarsalah=True)
    	d55=Choice.objects.create(pertanyaan="d5",choice="#",benarsalah=False)
    	Soal5=Pertanyaan.objects.create(no=5,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal5j/1/1/')
    	self.assertEqual(found.func,soal5j)

    def test_soal6s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a55=Choice.objects.create(pertanyaan="a6",choice="#",benarsalah=False)
    	b55=Choice.objects.create(pertanyaan="b6",choice="#",benarsalah=False)
    	c55=Choice.objects.create(pertanyaan="c6",choice="#",benarsalah=True)
    	d55=Choice.objects.create(pertanyaan="d6",choice="#",benarsalah=False)
    	Soal6=Pertanyaan.objects.create(no=6,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal6s/1/')
    	self.assertEqual(found.func,soal6s)

    def test_soal6j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a66=Choice.objects.create(pertanyaan="a6",choice="#",benarsalah=False)
    	b66=Choice.objects.create(pertanyaan="b6",choice="#",benarsalah=False)
    	c66=Choice.objects.create(pertanyaan="c6",choice="#",benarsalah=True)
    	d66=Choice.objects.create(pertanyaan="d6",choice="#",benarsalah=False)
    	Soal6=Pertanyaan.objects.create(no=6,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal6j/1/1/')
    	self.assertEqual(found.func,soal6j)

    def test_soal7s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a77=Choice.objects.create(pertanyaan="a7",choice="#",benarsalah=False)
    	b77=Choice.objects.create(pertanyaan="b7",choice="#",benarsalah=False)
    	c77=Choice.objects.create(pertanyaan="c7",choice="#",benarsalah=True)
    	d77=Choice.objects.create(pertanyaan="d7",choice="#",benarsalah=False)
    	Soal7=Pertanyaan.objects.create(no=7,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal7s/1/')
    	self.assertEqual(found.func,soal7s)

    def test_soal7j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a66=Choice.objects.create(pertanyaan="a7",choice="#",benarsalah=False)
    	b66=Choice.objects.create(pertanyaan="b7",choice="#",benarsalah=False)
    	c66=Choice.objects.create(pertanyaan="c7",choice="#",benarsalah=True)
    	d66=Choice.objects.create(pertanyaan="d7",choice="#",benarsalah=False)
    	Soal7=Pertanyaan.objects.create(no=7,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal7j/1/1/')
    	self.assertEqual(found.func,soal7j)

    def test_soal8s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a88=Choice.objects.create(pertanyaan="a8",choice="#",benarsalah=False)
    	b88=Choice.objects.create(pertanyaan="b8",choice="#",benarsalah=False)
    	c88=Choice.objects.create(pertanyaan="c8",choice="#",benarsalah=True)
    	d88=Choice.objects.create(pertanyaan="d8",choice="#",benarsalah=False)
    	Soal8=Pertanyaan.objects.create(no=8,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal8s/1/')
    	self.assertEqual(found.func,soal8s)

    def test_soal8j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a88=Choice.objects.create(pertanyaan="a8",choice="#",benarsalah=False)
    	b88=Choice.objects.create(pertanyaan="b8",choice="#",benarsalah=False)
    	c88=Choice.objects.create(pertanyaan="c8",choice="#",benarsalah=True)
    	d88=Choice.objects.create(pertanyaan="d8",choice="#",benarsalah=False)
    	Soal8=Pertanyaan.objects.create(no=8,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal8j/1/1/')
    	self.assertEqual(found.func,soal8j)


    def test_soal9s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a99=Choice.objects.create(pertanyaan="a9",choice="#",benarsalah=False)
    	b99=Choice.objects.create(pertanyaan="b9",choice="#",benarsalah=False)
    	c99=Choice.objects.create(pertanyaan="c9",choice="#",benarsalah=True)
    	d99=Choice.objects.create(pertanyaan="d9",choice="#",benarsalah=False)
    	Soal9=Pertanyaan.objects.create(no=9,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal9s/1')
    	self.assertEqual(found.func,soal9s)


    def test_soal9j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a99=Choice.objects.create(pertanyaan="a9",choice="#",benarsalah=False)
    	b99=Choice.objects.create(pertanyaan="b9",choice="#",benarsalah=False)
    	c99=Choice.objects.create(pertanyaan="c9",choice="#",benarsalah=True)
    	d99=Choice.objects.create(pertanyaan="d9",choice="#",benarsalah=False)
    	Soal9=Pertanyaan.objects.create(no=9,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal9j/1/1/')
    	self.assertEqual(found.func,soal9j)


    def test_soal10s_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a100=Choice.objects.create(pertanyaan="a10",choice="#",benarsalah=False)
    	b100=Choice.objects.create(pertanyaan="b10",choice="#",benarsalah=False)
    	c100=Choice.objects.create(pertanyaan="c10",choice="#",benarsalah=True)
    	d100=Choice.objects.create(pertanyaan="d10",choice="#",benarsalah=False)
    	Soal10=Pertanyaan.objects.create(no=10,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal10s/1/')
    	self.assertEqual(found.func,soal10s)


    def test_soal10j_func(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a100=Choice.objects.create(pertanyaan="a10",choice="#",benarsalah=False)
    	b100=Choice.objects.create(pertanyaan="b10",choice="#",benarsalah=False)
    	c100=Choice.objects.create(pertanyaan="c10",choice="#",benarsalah=True)
    	d100=Choice.objects.create(pertanyaan="d10",choice="#",benarsalah=False)
    	Soal10=Pertanyaan.objects.create(no=10,pertanyaan="#")
    	found = resolve('/TestPengetahuan/soal10j/1/1/')
    	self.assertEqual(found.func,soal10j)


    def test_hasil_lebih_dari_100_func(self):
    	nilai = Nilai.objects.create(nilai=110,nama="Guest")
    	found = resolve('/TestPengetahuan/hasil/1/')
    	self.assertEqual(found.func,hasil)

    def test_hasil_kurang_dari_100_func(self):
    	nilai = Nilai.objects.create(nilai=10,nama="Guest")
    	found = resolve('/TestPengetahuan/hasil/1/')
    	self.assertEqual(found.func,hasil)

    def test_hasil_login(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', password='test3030')    
        self.client.login(username='test', password='test3030')
        nilai = Nilai.objects.create(nilai=10,nama="Guest")
        response = self.client.get(reverse('TestPengetahuan:hasil',kwargs={"wid" : 1}))
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tekan disini bila soal Terakhir sudah tidak relevan", html_kembalian)
    
    def test_hasil_tidaklogin(self):
        self.client=Client()
        nilai = Nilai.objects.create(nilai=10,nama="Guest")
        response = self.client.get(('/TestPengetahuan/hasil/1/'))
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn("Tekan disini bila soal Terakhir sudah tidak relevan", html_kembalian)

    def test_model_benar(self):
    	nilai = Nilai.objects.create(nilai=0,nama="Guest")
    	a100=Choice.objects.create(pertanyaan="a10",choice="#",benarsalah=False)
    	b100=Choice.objects.create(pertanyaan="b10",choice="#",benarsalah=False)
    	c100=Choice.objects.create(pertanyaan="c10",choice="#",benarsalah=True)
    	d100=Choice.objects.create(pertanyaan="d10",choice="#",benarsalah=False)
    	Soal10=Pertanyaan.objects.create(no=10,pertanyaan="#")
    	jumlahnilai=Nilai.objects.all().count()
    	jumlahChoice=Choice.objects.all().count()
    	jumlahsoal=Pertanyaan.objects.all().count()
    	self.assertEqual(jumlahsoal,1)
    	self.assertEqual(jumlahnilai,1)
    	self.assertEqual(jumlahChoice,4)

    def test_leaderboard1_home(self):
        response = Client().get('/TestPengetahuan/home/')
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn('LeaderBoard', html_kembalian)
        self.assertTemplateUsed(response, 'test_pengetahuan/home.html')

    def test_leaderboard_login_home(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', password='test3030')    
        self.client.login(username='test', password='test3030')
        response = self.client.get(reverse('TestPengetahuan:home'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn('LeaderBoard', html_kembalian)
        self.assertTemplateUsed(response, 'test_pengetahuan/home.html')

    def test_leaderboard_hasil(self):
        nilai = Nilai.objects.create(nilai=10,nama="Guest")
        response = Client().get('/TestPengetahuan/hasil/1/')
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn('LeaderBoard', html_kembalian)
        self.assertTemplateUsed(response, 'hasil.html')

    def test_leaderboard_login_hasil(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', password='test3030')    
        self.client.login(username='test', password='test3030')
        nilai = Nilai.objects.create(nilai=10,nama="Guest")
        response = self.client.get(reverse('TestPengetahuan:hasil',kwargs={"wid" : 1}))
        html_kembalian = response.content.decode('utf8')
        self.assertIn('LeaderBoard', html_kembalian)
        self.assertTemplateUsed(response, 'hasil.html')

    def test_get_recommendation_json(self):
        request = HttpRequest()
        response = score(request)
        self.assertTrue(type(response)==JsonResponse)

    def test_ganti(self):
        response = Client().get('/TestPengetahuan/ganti/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'test_pengetahuan/home.html')

    def test_ganti_login(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', password='test3030')    
        self.client.login(username='test', password='test3030')
        response = self.client.get('/TestPengetahuan/ganti/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'test_pengetahuan/ganti.html')



      