from django.db import models
from django.conf import settings
# Create your models here.

class Pertanyaan(models.Model):
	no = models.IntegerField(default=1)
	pertanyaan = models.CharField(max_length=200)

class Choice(models.Model):
    pertanyaan = models.CharField(max_length=20)
    choice = models.CharField(max_length=50)
    benarsalah = models.BooleanField(default=False)

class Nilai(models.Model):
	nilai = models.IntegerField(default=0)
	nama = models.CharField(max_length=50)

class NilaiSimpan(models.Model):
	nilai = models.IntegerField(default=0)
	userrname=models.CharField(max_length=80,default="")
	userr= models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

class Ganti(models.Model):
	a=models.CharField(max_length=20,default="")
	b=models.CharField(max_length=20,default="")
	c=models.CharField(max_length=20,default="")
	d=models.CharField(max_length=20,default="")