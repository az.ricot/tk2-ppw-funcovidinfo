from django.shortcuts import render,redirect
from .models import *
from .models import Pertanyaan,Choice,Nilai,Ganti
from .forms import gantiform
from django.core import serializers
from django.http import JsonResponse,HttpResponse
import json

#from .models import kegiatan, anggota
# Create your views here.

def home(request):
	if Nilai.objects.all().count() != 10:
		Pertanyaan.objects.all().delete()
		Soal1=Pertanyaan.objects.create(no=1,pertanyaan="Virus COVID-19 dapat menular melalui berbagai cara,kecuali...")
		Soal2=Pertanyaan.objects.create(no=2,pertanyaan="Rentang umur mana yang paling rentan untuk terkena virus COVID-19")
		Soal3=Pertanyaan.objects.create(no=3,pertanyaan="Jenis masker mana yang dilarang oleh Kementerian Kesehatan")
		Soal4=Pertanyaan.objects.create(no=4,pertanyaan="Ada beberapa test yang dapat dilakukan untuk mengetahui apakah seseorang mengidap COVID-19, test yang paling terpercaya adalah")
		Soal5=Pertanyaan.objects.create(no=5,pertanyaan="Bagian tubuh mana yang dilarang disentuh oleh tangan kecuali")
		Soal6=Pertanyaan.objects.create(no=6,pertanyaan="Berapa lama masa inkubasi COVID-19")
		Soal7=Pertanyaan.objects.create(no=7,pertanyaan="Apa saja gejala paling umum orang terjangkit COVID-19")
		Soal8=Pertanyaan.objects.create(no=8,pertanyaan="Provinsi dengan total kasus terbanyak adalah provinsi")
		Soal9=Pertanyaan.objects.create(no=9,pertanyaan="Indonesia memiliki web resmi untuk menyebarkan tentang informasi , nama web-nya adalah")
		Soal10=Pertanyaan.objects.create(no=10,pertanyaan="Jika tidak ada air serta sabun, dapat menggunakan alcohol-based sanitizer dengan ...persen alcohol")
		Soal1.save()
		Soal2.save()
		Soal3.save()
		Soal4.save()
		Soal5.save()
		Soal6.save()
		Soal7.save()
		Soal8.save()
		Soal9.save()
		Soal10.save()
	if Choice.objects.all().count() != 38:
		Choice.objects.all().delete()
		Soal1 = Pertanyaan.objects.get(no=1)
		a11=Choice.objects.create(pertanyaan="a1",choice="Penularan percikan pernapasan",benarsalah=False)
		b11=Choice.objects.create(pertanyaan="b1",choice="Penularan kontak tidak langsung",benarsalah=False)
		c11=Choice.objects.create(pertanyaan="c1",choice="Penularan aerosol",benarsalah=True)
		d11=Choice.objects.create(pertanyaan="d1",choice="Penularan dari ibu ke anak",benarsalah=False)
		Soal2 = Pertanyaan.objects.get(no=2)
		a22=Choice.objects.create(pertanyaan="a2",choice="0-17 tahun",benarsalah=False)
		b22=Choice.objects.create(pertanyaan="b2",choice="18-39 tahun",benarsalah=False)
		c22=Choice.objects.create(pertanyaan="c2",choice="40-64 tahun",benarsalah=False)
		d22=Choice.objects.create(pertanyaan="d2",choice=">65 tahun",benarsalah=True)
		Soal3 = Pertanyaan.objects.get(no=3)
		a33=Choice.objects.create(pertanyaan="a3",choice="Masker Bedah",benarsalah=False)
		b33=Choice.objects.create(pertanyaan="b3",choice="Masker Scuba",benarsalah=True)
		c33=Choice.objects.create(pertanyaan="c3",choice="Masker Kain",benarsalah=False)
		d33=Choice.objects.create(pertanyaan="d3",choice="Masker N95",benarsalah=False)
		Soal4 = Pertanyaan.objects.get(no=4)
		a44=Choice.objects.create(pertanyaan="a4",choice="Rapid test",benarsalah=False)
		c44=Choice.objects.create(pertanyaan="c4",choice="Polymerase Chain Reaction (PCR)",benarsalah=True)
		Soal5 = Pertanyaan.objects.get(no=5)
		a55=Choice.objects.create(pertanyaan="a5",choice="Badan",benarsalah=True)
		b55=Choice.objects.create(pertanyaan="b5",choice="Mata",benarsalah=False)
		c55=Choice.objects.create(pertanyaan="c5",choice="Mulut",benarsalah=False)
		d55=Choice.objects.create(pertanyaan="d5",choice="Hidung",benarsalah=False)
		Soal6 = Pertanyaan.objects.get(no=6)
		a66=Choice.objects.create(pertanyaan="a6",choice="1-3 hari",benarsalah=False)
		b66=Choice.objects.create(pertanyaan="b6",choice=">30 hari",benarsalah=False)
		c66=Choice.objects.create(pertanyaan="c6",choice="1- 14 jam",benarsalah=False)
		d66=Choice.objects.create(pertanyaan="d6",choice="1-14 hari",benarsalah=True)
		Soal7 = Pertanyaan.objects.get(no=7)
		a77=Choice.objects.create(pertanyaan="a7",choice="Sakit kepala ,sakit tenggorokan",benarsalah=False)
		b77=Choice.objects.create(pertanyaan="b7",choice="Kelelahan,diare",benarsalah=False)
		c77=Choice.objects.create(pertanyaan="c7",choice="Demam,batuk kering,sesak napas",benarsalah=True)
		d77=Choice.objects.create(pertanyaan="d7",choice="Menggigil,bersin-bersin",benarsalah=False)
		Soal8 = Pertanyaan.objects.get(no=8)
		a88=Choice.objects.create(pertanyaan="a8",choice="Jawa Barat",benarsalah=False)
		b88=Choice.objects.create(pertanyaan="b8",choice="Jawa Timur",benarsalah=False)
		c88=Choice.objects.create(pertanyaan="c8",choice="DKI Jakarta",benarsalah=True)
		d88=Choice.objects.create(pertanyaan="d8",choice="Jawa Tengah",benarsalah=False)
		Soal9 = Pertanyaan.objects.get(no=9)
		a99=Choice.objects.create(pertanyaan="a9",choice="https://www.cdc.gov/",benarsalah=False)
		b99=Choice.objects.create(pertanyaan="b9",choice="https://covid19.go.id/",benarsalah=True)
		c99=Choice.objects.create(pertanyaan="c9",choice="https://COVID-19.go.id/",benarsalah=False)
		d99=Choice.objects.create(pertanyaan="d9",choice="https://www.who.int/",benarsalah=False)
		Soal10 = Pertanyaan.objects.get(no=10)
		a100=Choice.objects.create(pertanyaan="a10",choice="30",benarsalah=False)
		b100=Choice.objects.create(pertanyaan="b10",choice="40",benarsalah=False)
		c100=Choice.objects.create(pertanyaan="c10",choice="50",benarsalah=False)
		d100=Choice.objects.create(pertanyaan="d10",choice="60",benarsalah=True)
		a11.save()
		b11.save()
		c11.save()
		d11.save()
		a22.save()
		b22.save()
		c22.save()
		d22.save()
		a33.save()
		b33.save()
		c33.save()
		d33.save()
		a44.save()
		c44.save()
		a55.save()
		b55.save()
		c55.save()
		d55.save()
		a66.save()
		b66.save()
		c66.save()
		d66.save()
		a77.save()
		b77.save()
		c77.save()
		d77.save()
		a88.save()
		b88.save()
		c88.save()
		d88.save()
		a99.save()
		b99.save()
		c99.save()
		d99.save()
		a100.save()
		b100.save()
		c100.save()
		d100.save()
	return render(request,'test_pengetahuan/home.html')

def soal1s(request):
	nilai = Nilai.objects.create(nilai=0,nama="Guest")
	Soal = Pertanyaan.objects.get(no=1)
	a=Choice.objects.get(pertanyaan="a1")
	b=Choice.objects.get(pertanyaan="b1")
	c=Choice.objects.get(pertanyaan="c1")
	d=Choice.objects.get(pertanyaan="d1")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal1s.html',context)

def soal1j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	Soal = Pertanyaan.objects.get(no=1)
	a=Choice.objects.get(pertanyaan="a1")
	b=Choice.objects.get(pertanyaan="b1")
	c=Choice.objects.get(pertanyaan="c1")
	d=Choice.objects.get(pertanyaan="d1")
	nilai = Nilai.objects.get(id = wid)
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	context = {
		'Soal':Soal,
		'tmp' : tmp,
		'nilai' : nilai,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	return render(request,'soal1j.html',context)

def soal2s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=2)
	a=Choice.objects.get(pertanyaan="a2")
	b=Choice.objects.get(pertanyaan="b2")
	c=Choice.objects.get(pertanyaan="c2")
	d=Choice.objects.get(pertanyaan="d2")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal2s.html',context)

def soal2j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=2)
	a=Choice.objects.get(pertanyaan="a2")
	b=Choice.objects.get(pertanyaan="b2")
	c=Choice.objects.get(pertanyaan="c2")
	d=Choice.objects.get(pertanyaan="d2")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal2j.html',context)

def soal3s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=3)
	a=Choice.objects.get(pertanyaan="a3")
	b=Choice.objects.get(pertanyaan="b3")
	c=Choice.objects.get(pertanyaan="c3")
	d=Choice.objects.get(pertanyaan="d3")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal3s.html',context)

def soal3j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=3)
	a=Choice.objects.get(pertanyaan="a3")
	b=Choice.objects.get(pertanyaan="b3")
	c=Choice.objects.get(pertanyaan="c3")
	d=Choice.objects.get(pertanyaan="d3")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal3j.html',context)

def soal4s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=4)
	a=Choice.objects.get(pertanyaan="a4")
	c=Choice.objects.get(pertanyaan="c4")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'c' : c,
	}
	return render(request,'soal4s.html',context)

def soal4j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=4)
	a=Choice.objects.get(pertanyaan="a4")
	c=Choice.objects.get(pertanyaan="c4")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'c' : c,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal4j.html',context)

def soal5s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=5)
	a=Choice.objects.get(pertanyaan="a5")
	b=Choice.objects.get(pertanyaan="b5")
	c=Choice.objects.get(pertanyaan="c5")
	d=Choice.objects.get(pertanyaan="d5")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal5s.html',context)

def soal5j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=5)
	a=Choice.objects.get(pertanyaan="a5")
	b=Choice.objects.get(pertanyaan="b5")
	c=Choice.objects.get(pertanyaan="c5")
	d=Choice.objects.get(pertanyaan="d5")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal5j.html',context)

def soal6s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=6)
	a=Choice.objects.get(pertanyaan="a6")
	b=Choice.objects.get(pertanyaan="b6")
	c=Choice.objects.get(pertanyaan="c6")
	d=Choice.objects.get(pertanyaan="d6")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal6s.html',context)

def soal6j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=6)
	a=Choice.objects.get(pertanyaan="a6")
	b=Choice.objects.get(pertanyaan="b6")
	c=Choice.objects.get(pertanyaan="c6")
	d=Choice.objects.get(pertanyaan="d6")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal6j.html',context)

def soal7s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=7)
	a=Choice.objects.get(pertanyaan="a7")
	b=Choice.objects.get(pertanyaan="b7")
	c=Choice.objects.get(pertanyaan="c7")
	d=Choice.objects.get(pertanyaan="d7")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal7s.html',context)

def soal7j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=7)
	a=Choice.objects.get(pertanyaan="a7")
	b=Choice.objects.get(pertanyaan="b7")
	c=Choice.objects.get(pertanyaan="c7")
	d=Choice.objects.get(pertanyaan="d7")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal7j.html',context)

def soal8s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=8)
	a=Choice.objects.get(pertanyaan="a8")
	b=Choice.objects.get(pertanyaan="b8")
	c=Choice.objects.get(pertanyaan="c8")
	d=Choice.objects.get(pertanyaan="d8")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal8s.html',context)

def soal8j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=8)
	a=Choice.objects.get(pertanyaan="a8")
	b=Choice.objects.get(pertanyaan="b8")
	c=Choice.objects.get(pertanyaan="c8")
	d=Choice.objects.get(pertanyaan="d8")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal8j.html',context)

def soal9s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=9)
	a=Choice.objects.get(pertanyaan="a9")
	b=Choice.objects.get(pertanyaan="b9")
	c=Choice.objects.get(pertanyaan="c9")
	d=Choice.objects.get(pertanyaan="d9")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal9s.html',context)

def soal9j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=9)
	a=Choice.objects.get(pertanyaan="a9")
	b=Choice.objects.get(pertanyaan="b9")
	c=Choice.objects.get(pertanyaan="c9")
	d=Choice.objects.get(pertanyaan="d9")
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal9j.html',context)

def soal10s(request,wid):
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=10)
	a=Choice.objects.get(pertanyaan="a10")
	b=Choice.objects.get(pertanyaan="b10")
	c=Choice.objects.get(pertanyaan="c10")
	d=Choice.objects.get(pertanyaan="d10")
	context = {
		'nilai' : nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	}
	return render(request,'soal10s.html',context)

def soal10j(request,qid,wid):
	tmp = Choice.objects.get(id=qid)
	nilai = Nilai.objects.get(id=wid)
	Soal = Pertanyaan.objects.get(no=10)
	a=Choice.objects.get(pertanyaan="a10")
	b=Choice.objects.get(pertanyaan="b10")
	c=Choice.objects.get(pertanyaan="c10")
	d=Choice.objects.get(pertanyaan="d10")
	print()
	context = {
		'tmp' : tmp,
		'nilai':nilai,
		'Soal' : Soal,
		'a' : a,
		'b' : b,
		'c' : c,
		'd' : d,
	} 
	if tmp.benarsalah == True:
		nilai.nilai = nilai.nilai+10
		nilai.save()
	return render(request,'soal10j.html',context)

def hasil(request,wid):
	nilai = Nilai.objects.get(id=wid)
	if nilai.nilai>100:
		nilai.nilai=0
	if request.user.is_authenticated:
		current_user = request.user
		print(current_user.id)
		simpan = NilaiSimpan.objects.create(nilai=nilai.nilai,userr=current_user,userrname=current_user.get_username())
		simpan.save()
	context={
		'nilai':nilai.nilai,
	}
	return render(request,'hasil.html',context)

def ganti(request):
	if request.user.is_authenticated:
		return render(request,'test_pengetahuan/ganti.html')
	else:
		return render(request,'test_pengetahuan/home.html')

def gantiajax(request):

	if request.method == "POST":
		benar=request.POST['benar']
		salah1=request.POST['salah1']
		salah2=request.POST['salah2']
		salah3=request.POST['salah3']
		a=Choice.objects.get(pertanyaan="a8")
		b=Choice.objects.get(pertanyaan="b8")
		c=Choice.objects.get(pertanyaan="c8")
		d=Choice.objects.get(pertanyaan="d8")
		a.choice=salah1
		b.choice=salah2
		c.choice=benar
		d.choice=salah3
		a.save()
		b.save()
		c.save()
		d.save()
		return HttpResponse("")

def score(request):
	simpan=serializers.serialize('json', NilaiSimpan.objects.all())
	simpan=json.loads(simpan)
	sort=sorted(simpan, key=lambda k: k['fields'].get('nilai', 0), reverse=True)
	return JsonResponse(simpan,safe=False)