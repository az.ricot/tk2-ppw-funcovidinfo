from django.shortcuts import render
from forum.models import Color

def home(request):
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
    return render(request, 'main/home.html', {'warnaUser' : warnaUser})
    
def about(request):
    if request.user.is_authenticated:
        warnaUser = Color.objects.filter(user = request.user)
        if warnaUser:
            warnaUser = Color.objects.get(user = request.user)
    else:
        warnaUser = None
    return render(request, 'main/about.html', {'warnaUser' : warnaUser})