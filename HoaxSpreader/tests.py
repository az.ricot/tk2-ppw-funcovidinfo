from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import home, country
from .models import Pertanyaan, Opsi, Skor
from django.contrib.auth.models import User

# Create your tests here.

class HoaxSpreaderTest(TestCase):

    def test_url_home_login(self):  
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/HoaxSpreader/home/')
        self.assertEquals(response.status_code, 200)

    def test_url_home_no_login(self):  
        self.client = Client()
        response = self.client.get('/HoaxSpreader/home/')
        self.assertEquals(response.status_code, 302)

    def test_func_home(self):
        found = resolve('/HoaxSpreader/home/')
        self.assertEqual(found.func, home)

    def test_template_home(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/HoaxSpreader/home/')
        self.assertTemplateUsed(response, 'home.html')

    def test_model_pertanyaan(self):
        pertanyaan = Pertanyaan.objects.create(no='1', pertanyaan='p1')
        hitungjumlah=Pertanyaan.objects.all().count()
        
        self.assertEqual(pertanyaan.no, '1')
        self.assertEqual(pertanyaan.pertanyaan, 'p1')

        self.assertEqual(hitungjumlah,1)
    
    def test_model_opsi(self):
        pertanyaan = Pertanyaan.objects.create(no='1', pertanyaan='p1')
        opsi = Opsi.objects.create(pertanyaan=pertanyaan, opsi1='o1', opsi2='o2', benarsalah1=True, benarsalah2=False)
        hitungjumlah=Opsi.objects.all().count()
        
        self.assertEqual(opsi.pertanyaan, pertanyaan)
        self.assertEqual(opsi.opsi1, 'o1')
        self.assertEqual(opsi.opsi2, 'o2')
        self.assertEqual(opsi.benarsalah1, True)
        self.assertEqual(opsi.benarsalah2, False)

        self.assertEqual(hitungjumlah,1)

    def test_model_skor(self):
        skor = Skor.objects.create(skor=0)
        hitungjumlah=Skor.objects.all().count()

        self.assertEqual(skor.skor, 0)
        self.assertEqual(hitungjumlah,1)

    def test_url_country(self):  
        self.client = Client()
        response = self.client.get('/HoaxSpreader/country/')
        self.assertEquals(response.status_code, 200)

    def test_func_home(self):
        found = resolve('/HoaxSpreader/country/')
        self.assertEqual(found.func, country)

    def test_template_home(self):
        self.client = Client()
        response = self.client.get('/HoaxSpreader/country/')
        self.assertTemplateUsed(response, 'country.html')



