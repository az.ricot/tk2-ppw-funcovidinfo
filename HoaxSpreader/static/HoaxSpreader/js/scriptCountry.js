$('.search-button').click(() => getData())

$('.input-keyword').keypress((e) => {
    if (e.which == 13) {
        getData()
    }
})

// Functions

function generateRow(b) {
    let link = b.volumeInfo.infoLink
    let desc = b.volumeInfo.description != undefined ? b.volumeInfo.description : "No Description"
    let thumbnail = b.volumeInfo.imageLinks
    let imgCover = thumbnail != null ? thumbnail.smallThumbnail.toString() : $("meta[name='images']").attr("content") + "book-placeholder.png"; 
    let dataArray = [b.id, link, b.volumeInfo.title, imgCover].join("#^%|^#")
    return `<div class="box d-flex align-items-start p-2 border-top border-bottom">
                <div class="cover d-flex justify-content-center">
                    <img src="${imgCover}" alt="">
                </div>
                <div class="d-flex flex-column ml-1 ml-md-3 col-md-12 col-10">
                    <a href="${link}" class="text-dark" target="_blank">
                    <h3 class="text-truncate col-9 p-0 title">${b.volumeInfo.title}</h3></a>
                    <p class="desc mb-1 col-10 p-0">${desc}</p>
                </div>
            </div>`
}

function getData() { 
    $.get("https://www.googleapis.com/books/v1/volumes?q=" + $('.input-keyword').val())
    .done((results) => {
        $(".content").empty()
        let content = $(".content")
        if (results.totalItems == 0) {
            content.append("<h5>Buku yang dicari tidak ada</h5>")
        } else {
            let books = results.items
            let first = "<h4>Hasil Pencarian Buku:</h4>"
            content.append(first)
            books.forEach(b => {
                $.get($("meta[name='home']").attr("content") + 'buku/' + b.id)
                .done(e => content.append(generateRow(b)))
            })
        }
    }) 
}

