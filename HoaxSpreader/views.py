from django.shortcuts import render
from .models import Pertanyaan, Opsi, Skor
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

# Create your views here.
@login_required(login_url='userAuth:login')
def home(request):
    Pertanyaan.objects.all().delete()
    p1=Pertanyaan(no=1,pertanyaan="Kamu melihat ada banyak orang berolahraga menggunakan masker. Mweheheh, sepertinya kamu bisa menyebarkan hoax. Ufufufufu iyahahaha")
    p2=Pertanyaan(no=2,pertanyaan="“Pemerintah mewajibkan masker 3 lapis. Masker scuba dilarang”")
    p3=Pertanyaan(no=3,pertanyaan="“Duh aku takut bgt sama korona, ada obatnya ga sih?”")
    p4=Pertanyaan(no=4,pertanyaan="“Mau sholat Jum’at tapi korona :( “")
    p5=Pertanyaan(no=5,pertanyaan="“Duh bosen beut dah, mau keluar rumah.”")
    p1.save()
    p2.save()
    p3.save()
    p4.save()
    p5.save()

    Opsi.objects.all().delete()
    o1=Opsi(pertanyaan=p1,
    opsi1="Ngerasa sesak ga? Ada yg sampai meninggal lhoh. Sesekali buka aja, gapapa. Toh, bukan di ruangan tertutup. Asal jaga jarak aja sich.",
    opsi2="Pake masker sambil olahraga bikin sesak lhoh, bahkan ada yg sampai meninggal. Olahraga di rumah aja. Bnyk kok home workout yg bisa dilakukan.",
    opsi3="Kalau olahraga, pake masker scuba aja. Kan cuma selapis tuh, biar ga sesak. Terhindar dari kematian, jg terhindar dari covid.",
    benarsalah1=True,benarsalah2=False,benarsalah3=False)
    o2=Opsi(pertanyaan=p2,
    opsi1="Ide bisnis tu, pdhl 1 lapis aja cukup",
    opsi2="Nanti sesak lhoh pake masker berlapis2, ga nyaman",
    opsi3="Ide bisnis tuch. Pake aja masker scuba sekali 3",
    benarsalah1=True,benarsalah2=False,benarsalah3=False)
    o3=Opsi(pertanyaan=p3,
    opsi1="Vitamin C dpt menjaga ketahanan tubuh, boleh nih beli produk aku. Xixixi",
    opsi2="Eh ini lhoh ada jamu anti korona dari doktor amerika yang terkenal, manjur bgt sis.",
    opsi3="Masyaallah!!! Ini ada jamu anti korona dari ustad syekh abu kadrun. Sudah diracik dengan ilmu pengetahuan dan dibaca dgn do’a yang mustajab. Hasil penjualan akan di-infaq-an ke panti asuhan.",
    benarsalah1=False,benarsalah2=False,benarsalah3=True)
    o4=Opsi(pertanyaan=p4,
    opsi1="Asal patuh terhadap protokol kesehatan, gapapa sih. Pake masker 3 lapis, bawa sajadah sendiri. Jangan lupa sehabis sholat, ganti baju semua dan mandi.",
    opsi2="Korona itu nggak ada. Itu akal busuk pemerintah kafirun agar merusak akidah kita. Tidak apa-apa. Allah akan melindungi kita yang senantiasa bertawakal kepada-Nya. Hidup dan mati kan atas kehendak Allah.",
    opsi3="Sholat di rumah aja. Allah pasti mengerti kok.",
    benarsalah1=False,benarsalah2=True,benarsalah3=False)
    o5=Opsi(pertanyaan=p5,
    opsi1="Yang lain pasti juga bosan di rumah terus. Main among us yuk biar ga bosan. Kemajuan teknologi harus dimanfaatkan nih.",
    opsi2="Sama nih sis. Yuk kita nyabe. Asal mematuhi protokol kesehatan xixixi",
    opsi3="Dilihat-lihat sih kayaknya korona itu palsu deh. Kamu udah pernah kena korona belum? Padahal data yang terkena korona banyak dan semakin meningkat. Masa kita ga kena? Gue jg bingung sih apa yang direncanakan pemerintah. Gapapa, ayo kita jalan-jalan. Asal mematuhi protokol kesehatan.",
    benarsalah1=False,benarsalah2=False,benarsalah3=True)
    o1.save()
    o2.save()
    o3.save()
    o4.save()
    o5.save()

    Skor.objects.all().delete()

    context = {}
    context["idAwal"] = Pertanyaan.objects.first().id

    return render(request,'home.html', context)

@login_required(login_url='userAuth:login')
def pertanyaan(request, id, cek):
    # dictionary for initial data with  
    # field names as keys 
    context ={} 

    # add the dictionary during initialization 
    context["pertanyaan"] = Pertanyaan.objects.get(id = id)
    context["opsi"] = Opsi.objects.get(pertanyaan_id = id)
    
    context["idSkrg"] = Pertanyaan.objects.get(id = id).id
    context["lastID"] = Pertanyaan.objects.last().id 

    if context["idSkrg"] == context["lastID"]:
        context["nextID"] = "skor"
    else:
        context["nextID"] = Pertanyaan.objects.filter(id__gt = id)[0].id
    if context["pertanyaan"] != Pertanyaan.objects.first():    
        context["opsii"] = Opsi.objects.get(pertanyaan_id = id-1) 
    
        if cek == 1 :
            if context["opsii"].benarsalah1 == True :
                Skor.objects.create(skor = 1)
            else:
                Skor.objects.create(skor = 0)
        if cek == 2 :
            if context["opsii"].benarsalah2 == True :
                Skor.objects.create(skor = 1)
            else:
                Skor.objects.create(skor = 0)
        if cek == 3 :
            if context["opsii"].benarsalah3 == True :
                Skor.objects.create(skor = 1)
            else:
                Skor.objects.create(skor = 0)  
    else:
        pass

    return render(request, "soal.html", context)

@login_required(login_url='userAuth:login')
def skor(request,cek):
    context = {}
    context["opsi"] = Opsi.objects.get(pertanyaan_id = Pertanyaan.objects.last().id) 

    if cek == 1 :
        if context["opsi"].benarsalah1 == True :
            Skor.objects.create(skor = 1)
        else:
            Skor.objects.create(skor = 0)
    if cek == 2 :
        if context["opsi"].benarsalah2 == True :
            Skor.objects.create(skor = 1)
        else:
            Skor.objects.create(skor = 0)
    if cek == 3 :
        if context["opsi"].benarsalah3 == True :
            Skor.objects.create(skor = 1)
        else:
            Skor.objects.create(skor = 0)
    
    context["skorlist"] = Skor.objects.all()
    context["skor"]=0
    context["skormaks"]=0
    for i in context["skorlist"] :
        context["skor"]+=i.skor
        context["skormaks"]+=1
    if context["skormaks"]>5:
         context["skor"]=0

    return render(request, "skor.html", context)

def country(request):
    return render(request, 'country.html')

def listCountry(request):
    return render(request, 'listCountry.html')