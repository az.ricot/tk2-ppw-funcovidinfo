
# funcovidinfo

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

*To view this file in English, [click here][readme-en].*

## Daftar Isi

- [Daftar Isi](#daftar-isi)
- [Anggota](#anggota)
- [Link Herokuapp](#link-herokuapp)
- [Tentang Aplikasi](#tentang-aplikasi)
- [Daftar Fitur](#daftar-fitur)


## Anggota

Kelompok B12 PPW 2020/2021-1

Anggota:
- Athallah Rikza Ihsani (1906353523)
- Ferdi Fadillah (1906351083)
- Gerrad Natanael Daloma (1906398704)
- Michael Felix Haryono (1906398326)
- Rico Tadjudin (1906398364)

## Link Herokuapp

Link Herokuapp: funcovidinfo.herokuapp.com

## Tentang Aplikasi

FunCovidInfo adalah aplikasi yang berisi informasi tentang covid yang dipaparkan dengan cara yang menyenangkan, seperti dengan mini game dan quiz. Selain itu, aplikasi kami juga memiliki menu forum untuk berdiskusi lebih lanjut. Aplikasi web ini dapat dijadikan sebagai sarana untuk mengedukasi Masyarakat Indonesia terkait Covid-19, mulai dari fakta-fakta terkait Covid-19 sampai dengan hoaks-hoaks yang beredar terkait Covid-19. Selain itu, FunCovidInfo dapat dijadikan sebagai sarana bagi pemerintah untuk menyebarluaskan informasi terkini terkait COVID-19. 

## Daftar Fitur

1. Mini game (Hoax Spreader, Hoax Destroyer)
- Hoax Spreader
Akan diberikan narasi dan pilihan. Tugas kita adalah memilih pilihan yang se-hoax dan seberbahaya mungkin. Setelah submit pilihan, akan ada narasi dari pilihan yang kita pilih. Skor akan diakumulasi dari setiap soal kemudian diberi predikat.
= Hoax Destroyer
Akan diberi soal dan kita harus jawab soal dengan menghancurkan hoax. Jika benar, dapat poin. Jika salah, tidak dapat. Skor akan diakumulasi di akhir.

2. Test (hoax, pengetahuan COVID-19)
- Test Hoax
Pada test ini, user akan diberikan berita/statement terkait Covid-19 lalu user menentukan apakah berita/statement itu termasuk hoax atau tidak. Jawaban dari test ini berupa pilihan benar/salah. Setelah menjawab, user akan mendapatkan tampilan jawaban yang benar dan pada akhir test akan terdapat hasil keseluruhan.
- Test Pengetahuan COVID-19
Test ini akan memuat soal pilihan ganda tentang pengetahuan umum seputar  COVID-19. Jika mendapat nilai diatas 60 akan diminta untuk masuk ke Forum untuk membantu dalam menjawab pertanyaan di forum, jika kurang dari 60 akan diminta untuk mengikuti tes lain agar mendapat ilmu yang lain

3. Forum 
Forum Diskusi adalah sebuah fitur yang sangat berguna pada website kami. Forum Diskusi akan menjadi sarana komunikasi antar pengguna website. Selain sebagai tempat berbagi pengalaman dan pengetahuan, forum juga bisa menjadi tempat pengguna untuk beropini dan berdiskusi tentang berbagai informasi terkini Covid-19 dan masalah-masalahnya.


[pipeline-badge]: https://gitlab.com/az.ricot/tk2-ppw-funcovidinfo/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/az.ricot/tk2-ppw-funcovidinfo/badges/master/coverage.svg
[commits-gl]: hhttps://gitlab.com/az.ricot/tk2-ppw-funcovidinfo/-/commits/master
[readme-en]: README.en.md
[heroku-dashboard]: https://dashboard.heroku.com
[djecrety]: https://djecrety.ir
[account-settings]: https://dashboard.heroku.com/account
[chromedriver]: https://chromedriver.chromium.org/downloads
[homebrew]: https://brew.sh
[ticket-21227]: https://code.djangoproject.com/ticket/21227
[bypass-cache]: https://en.wikipedia.org/wiki/Wikipedia:Bypass_your_cache
[flake8]: https://pypi.org/project/flake8
[pylint]: https://pypi.org/project/pylint
[black]: https://pypi.org/project/black
[isort]: https://pypi.org/project/isort
[template]: https://docs.djangoproject.com/en/3.1/ref/django-admin/#cmdoption-startproject-template
[repo-gh]: https://github.com/laymonage/django-template-heroku
[repo-gl]: https://gitlab.com/laymonage/django-template-heroku
[license]: LICENSE

