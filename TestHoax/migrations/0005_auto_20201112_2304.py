# Generated by Django 3.1.1 on 2020-11-12 16:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TestHoax', '0004_auto_20201112_2240'),
    ]

    operations = [
        migrations.RenameField(
            model_name='soal',
            old_name='Jawaban',
            new_name='KunciJawaban',
        ),
        migrations.RemoveField(
            model_name='jawabanuser',
            name='JawabanInput',
        ),
        migrations.AddField(
            model_name='jawabanuser',
            name='Jawaban',
            field=models.CharField(choices=[('select1', 'True'), ('select2', 'False')], default='select1', max_length=7),
        ),
    ]
